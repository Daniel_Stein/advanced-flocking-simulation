﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu_UiManager : UiManagerBase
{
    [SerializeField] GameObject loadMap_Panel;
    [SerializeField] RectTransform loadMapScrollContent;
    [SerializeField] GameObject loadMapEntrance_Prefab;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 1;
    }

    public void CreateLoadList()
    {
        MapSaverLoader.SavedMapFiles[] savedFiles = MapSaverLoader.getSavedMapList();

        openPanelFading(loadMap_Panel.GetComponent<CanvasGroup>());
        loadMapScrollContent.sizeDelta = new Vector2(0, savedFiles.Length * 40 + 10);

        for (int i = 1; i < loadMapScrollContent.childCount; i++)
        {
            Destroy(loadMapScrollContent.GetChild(i).gameObject);
        }

        foreach (var file in savedFiles)
        {
            GameObject temp = Instantiate(loadMapEntrance_Prefab, loadMapScrollContent);
            temp.GetComponentInChildren<Text>().text = file.mapName;
            temp.SetActive(true);
        }
    }

    public void saveChoosenMap(Text MapName_Text)
    {
        PlayerPrefs.SetString("MapToLoad", MapName_Text.text);
    }

    public void openPanelFading(CanvasGroup CV)
    {
        LeanTween.alphaCanvas(CV, 1, 0.1f);
        CV.interactable = true;
    }

    public void closePanelFading(CanvasGroup CV)
    {
        LeanTween.alphaCanvas(CV, 0, 0.15f);
        CV.interactable = false;
    }
}
