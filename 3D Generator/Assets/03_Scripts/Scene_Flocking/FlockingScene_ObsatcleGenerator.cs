﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FlockingScene_ObsatcleGenerator : ObstacleGenerator
{
    [Header("Flocking Scene References")]
    [SerializeField] NavMeshSurface navMeshSurface;
    [SerializeField] BorderGenerator borderGenerator;
    [SerializeField] FlockingScene_UiManager flockingUiManager;

    protected override void beforeProcess()
    {
        
    }

    protected override void afterProcess()
    {
        obstaclePainter.showObsatclePoints(false);
        navMeshSurface.BuildNavMesh();

        ClosestPointCalculator.setAreaSize(borderGenerator.areaSize);
        ClosestPointCalculator.setBorderList(obstaclePointGroups);

        flockingUiManager.ActPlayMode = PlayMode.Editing;
    }

}
