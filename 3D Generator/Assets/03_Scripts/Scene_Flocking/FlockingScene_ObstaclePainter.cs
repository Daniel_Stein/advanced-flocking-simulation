﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Is not an Obstacle Painter. It paints the Start-Point and End-Point for the Flock.
/// </summary>
public class FlockingScene_ObstaclePainter : ObstaclePainter
{

    [Header("Script References")]
    [SerializeField] FlockSpawner flockSpawner;

    protected override void Update()
    {
        if (playerCon.cameraMode == CameraModes.TopDownView && uiManger.ActPlayMode == PlayMode.Editing && mouseIsOnObject)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit);

            if (Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKey(KeyCode.Mouse0))
            {
                placeStartPoint(hit.point);
            }
            else if (Input.GetKeyDown(KeyCode.Mouse1) || Input.GetKey(KeyCode.Mouse1))
            {
                placeGoalPoint(hit.point);
            }
        }
    }

    void placeStartPoint(Vector3 Position)
    {
        if (!hasPaintedPoint(Position, Flock.spawnCircleRadius, out Vector2Int[] affectedPoints))
        {
            Position.x = (int)(Position.x) + 0.5f;
            Position.z = (int)(Position.z) + 0.5f;

            flockSpawner.SpawnPosition = Position + new Vector3(0, 0.1f, 0);
            Flock.spawnPositions = affectedPoints;
        }
    }

    void placeGoalPoint(Vector3 Position)
    {
        flockSpawner.GoalPosition = Position + new Vector3(0, 0.1f, 0);
    }

    public bool hasPaintedPoint(Vector3 pointerPos, int Radius, out Vector2Int[] affectedPoints)
    {
        Vector2Int roundedPos = new Vector2Int(((int)pointerPos.x), ((int)pointerPos.z));
        int minX = roundedPos.x - (Radius - 1);
        int maxX = roundedPos.x + Radius;
        int minY = roundedPos.y - (Radius - 1);
        int maxY = roundedPos.y + Radius;

        affectedPoints = getAffectedPoints(pointerPos, Radius);

        if (minX <= 0 || maxX >= borderGenerator.areaSize.x || minY <= 0 || maxY >= borderGenerator.areaSize.y)
        {
            return true;
        }
        foreach (Vector2Int point in affectedPoints)
        {
            if (pointRaster.findPoint(point) != null)
            {
                return true;
            }
        }

        return false;
    }

    public Vector2Int[] getAffectedPoints(Vector3 pointerPos, int circleRadius)
    {
        setBrushForm(BrushForm.Round);
        ushort brushSize = (ushort)(circleRadius * 2);
        setBrushSize(ref brushSize);

        return getBrushedArea(pointerPos);
    }
}
