﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CreateFlockingArena : MonoBehaviour
{
    [Header("Script References")]
    [SerializeField] BorderGenerator borderGenerator;
    [SerializeField] ObstacleGenerator obstacleGenerator;
    [SerializeField] ObstaclePainter obstaclePainter;
    [SerializeField] FlockingScene_UiManager uiManager;

    // Start is called before the first frame update
    void Start()
    {
        uiManager.ActPlayMode = PlayMode.Generating;

        string path = File.ReadAllText(Application.streamingAssetsPath + "/" + PlayerPrefs.GetString("MapToLoad") + ".json");
        MapSaverLoader.SaveDataHolder saveData = JsonUtility.FromJson<MapSaverLoader.SaveDataHolder>(path);

        int areaSizeX = (int)saveData.AreaSize.x;
        int areaSizeY = (int)saveData.AreaSize.y;
        borderGenerator.GenerateNewArena(ref areaSizeX, ref areaSizeY);

        obstaclePainter.setObsatclePointArraySize();
        foreach (Vector2Int point in saveData.PaintedPoints)
        {
            obstaclePainter.paintSingleObsatclePoint(PaintTool.Draw, point);
        }

        obstacleGenerator.ObstacleHeight = saveData.ObstacleHeight;
        obstacleGenerator.setBreakTime(0);

        obstacleGenerator.StartGeneratingObsatcles();
    }
}
