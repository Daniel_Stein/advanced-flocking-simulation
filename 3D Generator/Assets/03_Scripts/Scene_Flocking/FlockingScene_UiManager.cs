﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlockingScene_UiManager : UiManagerBase
{
    [Header("UI References")]
    [SerializeField] UI_BoolBox startPointPlaced_BoolBox;
    [SerializeField] UI_BoolBox goalPointPlaced_BoolBox;
    [SerializeField] Slider spawnCircleRadius_Slider;
    [SerializeField] Text spawnCircleRadius_Text;
    [SerializeField] Slider spawnCount_Slider;
    [SerializeField] Text spawnCount_Text;
    [SerializeField] Toggle[] flockType_Toggles = new Toggle[3];
    [SerializeField] Button spawnFlock_Button;
    [SerializeField] Button deleteFlock_Button;
    [SerializeField] UI_InfoBox flockInfo_InfoBox;

    [Header("Script References")]
    [SerializeField] FlockSpawner flockSpawner;


    private void Start()
    {
        getPlayerPrefsAtStart();
    }

    void getPlayerPrefsAtStart()
    {
        setStartCircleRadiusAtStart(PlayerPrefs.GetInt("StartCircleRadius", 2));

        int flockTypeNumber = PlayerPrefs.GetInt("FlockTypeNumber", 0);
        flockType_Toggles[flockTypeNumber].isOn = true;
        setFlockType(flockTypeNumber);

        int spawnCount = PlayerPrefs.GetInt("SpawnCount", 1);
        spawnCount_Slider.value = spawnCount;
        setSpawnCount(spawnCount);
    }


    #region Set Data

    public void showIfStartPointIsPlaced(bool isPlaced)
    {
        startPointPlaced_BoolBox.Value = isPlaced;
        canSpawnAgents[1] = isPlaced;
        activateSpawnButton();
    }

    public void showIfGoalPointIsPlaced(bool isPlaced)
    {
        goalPointPlaced_BoolBox.Value = isPlaced;
        canSpawnAgents[2] = isPlaced;
        activateSpawnButton();
    }

    public void setStartCircleRadius(float Radius)
    {
        int temp = flockSpawner.setSpawnCircleRadius((int)Radius);
        spawnCircleRadius_Slider.value = temp;
        spawnCircleRadius_Text.text = "" + temp;
        PlayerPrefs.SetInt("StartCircleRadius", temp);
    }

    public void setStartCircleRadiusAtStart(float Radius)
    {
        int temp = flockSpawner.setSpawnCircleRadiusAtStart((int)Radius);
        spawnCircleRadius_Slider.value = temp;
        spawnCircleRadius_Text.text = "" + temp;
        PlayerPrefs.SetInt("StartCircleRadius", temp);
    }

    public void setSpawnCount(float SpawnCount)
    {
        flockSpawner.SpawnCount = (int)SpawnCount;
        spawnCount_Text.text = "" + (int)SpawnCount;
        PlayerPrefs.SetInt("SpawnCount", (int)SpawnCount);
    }

    public void setFlockType(int typeNumber)
    {
        PlayerPrefs.SetInt("FlockTypeNumber", typeNumber);
        flockSpawner.setFlockType(typeNumber);
    }

    public void spawnFlock()
    {
        flockSpawner.spawnFlock();
    }

    public void deleteFlock()
    {
        flockSpawner.deleteFlock();
    }

    public void showPureFlockingInfo()
    {
        Vector2 position = Input.mousePosition;
        float windowHeight = 140;
        string titleText = "Pure Flocking";
        string infoText = "This flock is simulated using the classic flocking algorithm. It consists of the flockintern behaviours: cohesion, avoidance and alignment. Additionally, the two environmental influences: obstacle avoidance and target-direction.";

        flockInfo_InfoBox.showInfoBox(position, windowHeight, titleText, infoText);
    }

    public void showPurePathfindingInfo()
    {
        Vector2 position = Input.mousePosition;
        float windowHeight = 115;
        string titleText = "Pure Pathfinding";
        string infoText = "This flock is simulated using only pathfinding, which is provided by Unity. Every member of the flock finds the shortest way towards the goal and moves on that calculated path.";

        flockInfo_InfoBox.showInfoBox(position, windowHeight, titleText, infoText);
    }

    public void showAdvancedFlockingInfo()
    {
        Vector2 position = Input.mousePosition;
        float windowHeight = 185;
        string titleText = "Advanced Flocking";
        string infoText = "This flock combines the two flock-types from above. It has one leader, that uses pathfinding to find the best way towards the target. All the other agents use the pure flocking algorithm to follow that leader. If they lose their direct connection, they are lining themselves up to a chain that connects them indirectly to the leader.";

        flockInfo_InfoBox.showInfoBox(position, windowHeight, titleText, infoText);
    }

    public void showNoSpawningInfo()
    {
        if (!canSpawnAgents[0] || !canSpawnAgents[1] || !canSpawnAgents[2])
        {
            Vector2 position = Input.mousePosition;
            float windowHeight = 95;
            string titleText = "Unable to Spawn";
            string infoText = "Before you can spawn a flock you have to:\n- wait until the map is generated\n- place the start-point\n- place the end-point";

            flockInfo_InfoBox.showInfoBox(position, windowHeight, titleText, infoText);
        }
    }

    public void hideInfoPanel()
    {
        flockInfo_InfoBox.hideInfoBox();
    }

    #endregion


    bool[] canSpawnAgents = new bool[3] { false, false, false };
    void activateSpawnButton()
    {
        spawnFlock_Button.interactable = (canSpawnAgents[0] && canSpawnAgents[1] && canSpawnAgents[2]);
    }


    public override PlayMode ActPlayMode
    {
        get { return actPlayMode; }
        set
        {
            actPlayMode = value;

            switch (value)
            {
                case PlayMode.Editing:
                    canSpawnAgents[0] = true;
                    activateSpawnButton();
                    deleteFlock_Button.interactable = true;
                    break;
                case PlayMode.Generating:
                    spawnFlock_Button.interactable = false;
                    deleteFlock_Button.interactable = false;
                    break;
                default:
                    break;
            }
        }
    }
}

