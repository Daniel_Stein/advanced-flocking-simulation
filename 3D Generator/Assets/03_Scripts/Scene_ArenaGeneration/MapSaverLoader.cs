﻿using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using UnityEditor;
using UnityEngine;

public class MapSaverLoader : MonoBehaviour
{
    [Header("Script References")]
    [SerializeField] ObstaclePainter obsPainter;
    [SerializeField] ObstacleGenerator obsGenerator;
    [SerializeField] BorderGenerator borGenerator;

    MapSaveMode saveMode = MapSaveMode.Json;


    #region Load Map

    public static SavedMapFiles[] getSavedMapList()
    {
        string[] savedFilePaths = Directory.GetFiles(Application.streamingAssetsPath, "*Map*.json");
        SavedMapFiles[] savedFiles = new SavedMapFiles[savedFilePaths.Length];

        for (int i = 0; i < savedFiles.Length; i++)
        {
            savedFiles[i].filePath = savedFilePaths[i];
            int nameReg = savedFilePaths[i].IndexOf("Map");
            savedFiles[i].mapName = savedFilePaths[i].Substring(nameReg, savedFilePaths[i].Length - nameReg - 5);
        }

        return savedFiles;
    }

    public void loadMap(string mapName, out Vector2 areaSize, out List<Vector2Int> paintedPoints, out float obstacleHeight)
    {
        string path = File.ReadAllText(Application.streamingAssetsPath + "/" + mapName + ".json");
        SaveDataHolder saveData = JsonUtility.FromJson<SaveDataHolder>(path);

        areaSize = saveData.AreaSize;
        paintedPoints = saveData.PaintedPoints;
        obstacleHeight = saveData.ObstacleHeight;
    }

    #endregion

    #region Save Map

    public string saveMap()
    {
        if (saveMode == MapSaveMode.Json)
        {
            return saveMapInJson();
        }
        else
        {
            saveMapInUnity();
            return "Editor only: Map saved.";
        }
    }

    string saveMapInJson()
    {
        SaveDataHolder saveData = new SaveDataHolder();
        saveData.AreaSize = borGenerator.areaSize;
        saveData.ObstacleHeight = obsGenerator.ObstacleHeight;
        foreach (ObstaclePoint point in obsPainter.PaintedPoints.getPointList())
        {
            saveData.PaintedPoints.Add(point.rasterPos);
        }

        string saveText = JsonUtility.ToJson(saveData);
        string path = Application.streamingAssetsPath + "/Map_" + getSaveNumber() + ".json";
        if (!File.Exists(path))
        {
            File.WriteAllText(path, saveText);
        }

        return path;
    }

    void saveMapInUnity()
    {
#if UNITY_EDITOR
        CombineInstance[] combine = new CombineInstance[3];
        MeshFilter[] meshFilters = new MeshFilter[3];
        meshFilters[0] = obsGenerator.GetComponent<MeshFilter>();
        meshFilters[1] = obsPainter.GetComponent<MeshFilter>();
        meshFilters[2] = borGenerator.GetComponent<MeshFilter>();

        for (int i = 0; i < 3; i++)
        {
            combine[i].mesh = meshFilters[i].mesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
        }

        Mesh combinedMesh = new Mesh();
        combinedMesh.name = "Map_" + getSaveNumber();
        combinedMesh.RecalculateNormals();
        combinedMesh.CombineMeshes(combine);

        AssetDatabase.CreateAsset(combinedMesh, "Assets/08_CreatedMeshes/" + combinedMesh.name + ".asset");
#endif
    }

    int getSaveNumber()
    {
        int meshNumber = PlayerPrefs.GetInt("SavedMeshNumber", 0);
        PlayerPrefs.SetInt("SavedMeshNumber", ++meshNumber);

        return meshNumber;
    }

    #endregion


    // User Functions
    public void setSaveMode(MapSaveMode mode)
    {
        saveMode = mode;
    }


    //Data Structures
    public enum MapSaveMode
    {
        Json, InsideUnity
    }

    public class SaveDataHolder
    {
        public Vector2 AreaSize = Vector2.zero;
        public List<Vector2Int> PaintedPoints = new List<Vector2Int>();
        public float ObstacleHeight = 1;
    }

    public struct SavedMapFiles
    {
        public string mapName;
        public string filePath;
    }
}