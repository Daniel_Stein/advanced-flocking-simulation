﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class ObstaclePainter : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Header("Object References")]
    [SerializeField] GameObject PointerPrefab;
    [SerializeField] Transform PointsSpawnHolder;
    [SerializeField] GameObject SelecterRingPrefab;
    [SerializeField] Transform SelecterRingSpawnHolder;

    [Header("Script References")]
    [SerializeField] protected BorderGenerator borderGenerator;
    [SerializeField] protected UiManagerBase uiManger;
    [SerializeField] protected PlayerController playerCon;

    // Priavte Values
    protected bool mouseIsOnObject = true;
    ushort brushSize = 1;
    BrushForm brushForm = BrushForm.Square;
    protected ObstaclePointRaster pointRaster;
    List<GameObject> spawnedSelecterRings = new List<GameObject>();

    protected virtual void Update()
    {
        if (playerCon.cameraMode == CameraModes.TopDownView && uiManger.ActPlayMode == PlayMode.Editing && mouseIsOnObject)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit);

            if (Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKey(KeyCode.Mouse0))
            {
                brushTasks(PaintTool.Draw, hit.point);
            }
            else if (Input.GetKeyDown(KeyCode.Mouse1) || Input.GetKey(KeyCode.Mouse1))
            {
                brushTasks(PaintTool.Erase, hit.point);
            }
            else
            {
                brushTasks(PaintTool.None, hit.point);
            }
        }
    }


    #region Paint Functions

    void brushTasks(PaintTool usedTool, Vector3 pointerPosition)
    {
        Vector2Int[] affectedPoints = getBrushedArea(pointerPosition);

        setSelectionRingPositions(affectedPoints, usedTool);

        if (usedTool != PaintTool.None)
        {
            foreach (Vector2Int position in affectedPoints)
            {
                paintSingleObsatclePoint(usedTool, position);
            }
        }
    }

    protected Vector2Int[] getBrushedArea(Vector3 position)
    {
        List<Vector2Int> back = new List<Vector2Int>(); ;

        Vector2 pointerPos = My_Math.ToVector2D(position);
        setBrushAreaBorders(out int xStart, out int xEnd, out int yStart, out int yEnd, pointerPos);

        for (int x = xStart; x <= xEnd; x++)
        {
            for (int y = yStart; y <= yEnd; y++)
            {
                if (brushForm == BrushForm.Round)
                {
                    if (Vector2.Distance(getMiddlePoint(pointerPos), new Vector2(x, y)) <= (float)brushSize / 2f)
                        back.Add(new Vector2Int(x, y));
                }
                else
                {
                    back.Add(new Vector2Int(x, y));
                }
            }
        }

        return back.ToArray();
    }

    void setBrushAreaBorders(out int xStart, out int xEnd, out int yStart, out int yEnd, Vector2 pointerPos)
    {
        int value = brushSize / 2;
        if (brushSize % 2 == 0)
        {
            Vector2Int roundedPos = new Vector2Int((int)(pointerPos.x), (int)(pointerPos.y));

            xStart = roundedPos.x - (value - 1);
            xEnd = roundedPos.x + value;
            yStart = roundedPos.y - (value - 1);
            yEnd = roundedPos.y + value;
        }
        else
        {
            Vector2Int roundedPos = new Vector2Int(Mathf.RoundToInt(pointerPos.x), Mathf.RoundToInt(pointerPos.y));

            xStart = roundedPos.x - value;
            xEnd = roundedPos.x + value;
            yStart = roundedPos.y - value;
            yEnd = roundedPos.y + value;
        }

        xStart = xStart < 0 ? 0 : xStart;
        xEnd = xEnd > borderGenerator.areaSize.x ? (int)borderGenerator.areaSize.x : xEnd;
        yStart = yStart < 0 ? 0 : yStart;
        yEnd = yEnd > borderGenerator.areaSize.y ? (int)borderGenerator.areaSize.y : yEnd;
    }

    Vector2 getMiddlePoint(Vector2 pointerPos)
    {
        Vector2 back = new Vector2(0, 0);

        if (brushSize % 2 == 0)
        {
            back.x = (int)(pointerPos.x) + 0.5f;
            back.y = (int)(pointerPos.y) + 0.5f;
        }
        else
        {
            back.x = Mathf.RoundToInt(pointerPos.x);
            back.y = Mathf.RoundToInt(pointerPos.y);
        }

        return back;
    }

    public void paintSingleObsatclePoint(PaintTool usedTool, Vector2Int position)
    {
        if (usedTool == PaintTool.Draw)
        {
            if (pointRaster.findPoint(position) == null)
            {
                ObstaclePoint temp = new ObstaclePoint(position, PointerPrefab);
                temp.placeHolder.transform.parent = PointsSpawnHolder;
                pointRaster.addPoint(temp);
            }
        }
        else
        {
            ObstaclePoint tempPoint = pointRaster.findPoint(position);
            if (tempPoint != null)
            {
                tempPoint.deletePlaceHolder();
                pointRaster.deletePoint(position);
            }
        }
    }

    void setSelectionRingPositions(Vector2Int[] positions, PaintTool usedTool)
    {
        int oldPointsCount = spawnedSelecterRings.Count;

        for (int i = 0; i < positions.Length; i++)
        {
            GameObject temp;
            if (i < oldPointsCount)
            {
                temp = spawnedSelecterRings[i];
                activateSelecter(temp);
                temp.transform.position = new Vector3(positions[i].x, 0.02f, positions[i].y);
            }
            else
            {
                temp = Instantiate(SelecterRingPrefab, new Vector3(positions[i].x, 0.02f, positions[i].y), Quaternion.Euler(90, 0, 0));
                activateSelecter(temp);
                temp.transform.parent = SelecterRingSpawnHolder;
                spawnedSelecterRings.Add(temp);
            }
            colorSelecter(temp, usedTool);
        }

        for (int i = positions.Length; i < oldPointsCount; i++)
        {
            deactivateSelecter(spawnedSelecterRings[i]);
        }
    }

    #endregion


    #region User Functions

    public void setObsatclePointArraySize()
    {
        int areaSizeX = (int)borderGenerator.areaSize.x + 1;
        int areaSizeY = (int)borderGenerator.areaSize.y + 1;

        ObstaclePoint[,] tempArray = new ObstaclePoint[areaSizeX, areaSizeY];

        if (pointRaster == null || !pointRaster.hasPoints)
        {
            pointRaster = new ObstaclePointRaster(new Vector2Int(0, 0), new Vector2Int(areaSizeX, areaSizeY));
        }
        else
        {
            int oldSizeX = pointRaster.arraySize.x;
            int oldSizeY = pointRaster.arraySize.y;
            int newSizeX = tempArray.GetLength(0);
            int newSizeY = tempArray.GetLength(1);

            for (int xPos = 0; xPos < oldSizeX; xPos++)
            {
                for (int yPos = 0; yPos < oldSizeY; yPos++)
                {
                    if (xPos < newSizeX && yPos < newSizeY)
                    {
                        tempArray[xPos, yPos] = pointRaster.findPoint(xPos, yPos);
                    }
                    else
                    {
                        if (pointRaster.findPoint(xPos, yPos) != null) pointRaster.deletePoint(xPos, yPos);
                    }
                }
            }

            pointRaster = new ObstaclePointRaster(new Vector2Int(0, 0), tempArray);
        }
    }

    public void DeleteAllPoints()
    {
        if (pointRaster != null) pointRaster.deleteAllPoints();
    }

    public void setBrushForm(BrushForm Form)
    {
        brushForm = Form;
    }

    public void setBrushSize(ref ushort Size)
    {
        Size = Size < 1 ? (ushort)1 : Size;
        Size = Size > 30 ? (ushort)30 : Size;

        brushSize = Size;
    }

    public void showObsatclePoints(bool showPoints)
    {
        PointsSpawnHolder.gameObject.SetActive(showPoints);
    }

    #endregion


    #region Mouse Input

    public void OnPointerEnter(PointerEventData eventData)
    {
        mouseIsOnObject = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mouseIsOnObject = false;

        foreach (GameObject rings in spawnedSelecterRings)
        {
            deactivateSelecter(rings);
        }
    }

    void activateSelecter(GameObject Selecter)
    {
        Selecter.SetActive(true);
        Selecter.GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(1, 1, 1));
    }

    void deactivateSelecter(GameObject Selecter)
    {
        Selecter.SetActive(false);
        Selecter.transform.position = new Vector3(-2, -2, -2);
    }

    void colorSelecter(GameObject Selecter, PaintTool usedTool)
    {
        switch (usedTool)
        {
            case PaintTool.Draw:
                Selecter.GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(0, 0.5f, 0.24f));
                break;
            case PaintTool.Erase:
                Selecter.GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(1, 0.11f, 0.2f));
                break;
            case PaintTool.None:
                Selecter.GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(0.95f, 0.95f, 0));
                break;
            default:
                break;
        }
    }

    #endregion


    //Getter Setter
    public ObstaclePointRaster PaintedPoints
    {
        get { return pointRaster; }
    }
}

public enum PaintTool
{
    Draw, Erase, None
}

public enum BrushForm
{
    Square, Round
}
