﻿using System.Collections.Generic;
using UnityEngine;

public class BorderGenerator : MeshGenerator
{
    [Header("Object References")]
    [SerializeField] GameObject FloorQuad;

    [Header("Editor Values")]
    public Vector2 areaSize;

    void createArena()
    {
        FloorQuad.transform.position = new Vector3(areaSize.x / 2, 0, areaSize.y / 2);
        FloorQuad.transform.localScale = new Vector3(areaSize.x, areaSize.y, 1);
        //FloorQuad.transform.localScale = new Vector3(areaSize.x / 10, 1, areaSize.y / 10);

        startGeneratingMesh("AreaWalls_" + areaSize.x + "_" + areaSize.y);

        //Floor
        //CreateConvexQuad(new Vector3(0, 0, 0), new Vector3(areaSize.x, 0, 0), new Vector3(0, 0, areaSize.y), new Vector3(areaSize.x, 0, areaSize.y));
        //InnerWalls
        addConvexQuad(new Vector3(0, 0, 0), new Vector3(0, 0, areaSize.y), new Vector3(0, 1, 0), new Vector3(0, 1, areaSize.y));
        addConvexQuad(new Vector3(0, 0, areaSize.y), new Vector3(areaSize.x, 0, areaSize.y), new Vector3(0, 1, areaSize.y), new Vector3(areaSize.x, 1, areaSize.y));
        addConvexQuad(new Vector3(areaSize.x, 0, areaSize.y), new Vector3(areaSize.x, 0, 0), new Vector3(areaSize.x, 1, areaSize.y), new Vector3(areaSize.x, 1, 0));
        addConvexQuad(new Vector3(areaSize.x, 0, 0), new Vector3(0, 0, 0), new Vector3(areaSize.x, 1, 0), new Vector3(0, 1, 0));
        //WallTop
        addConvexQuad(new Vector3(0, 1, 0), new Vector3(0, 1, areaSize.y), new Vector3(-1, 1, -1), new Vector3(-1, 1, areaSize.y + 1));
        addConvexQuad(new Vector3(0, 1, areaSize.y), new Vector3(areaSize.x, 1, areaSize.y), new Vector3(-1, 1, areaSize.y + 1), new Vector3(areaSize.x + 1, 1, areaSize.y + 1));
        addConvexQuad(new Vector3(areaSize.x, 1, areaSize.y), new Vector3(areaSize.x, 1, 0), new Vector3(areaSize.x + 1, 1, areaSize.y + 1), new Vector3(areaSize.x + 1, 1, -1));
        addConvexQuad(new Vector3(areaSize.x, 1, 0), new Vector3(0, 1, 0), new Vector3(areaSize.x + 1, 1, -1), new Vector3(-1, 1, -1));
        //OuterWalls
        addConvexQuad(new Vector3(-1, 0, areaSize.y + 1), new Vector3(-1, 0, -1), new Vector3(-1, 1, areaSize.y + 1), new Vector3(-1, 1, -1));
        addConvexQuad(new Vector3(areaSize.x + 1, 0, areaSize.y + 1), new Vector3(-1, 0, areaSize.y + 1), new Vector3(areaSize.x + 1, 1, areaSize.y + 1), new Vector3(-1, 1, areaSize.y + 1));
        addConvexQuad(new Vector3(areaSize.x + 1, 0, -1), new Vector3(areaSize.x + 1, 0, areaSize.y + 1), new Vector3(areaSize.x + 1, 1, -1), new Vector3(areaSize.x + 1, 1, areaSize.y + 1));
        addConvexQuad(new Vector3(-1, 0, -1), new Vector3(areaSize.x + 1, 0, -1), new Vector3(-1, 1, -1), new Vector3(areaSize.x + 1, 1, -1));
        //WallFloor
        addConvexQuad(new Vector3(0, 0, areaSize.y), new Vector3(0, 0, 0), new Vector3(-1, 0, areaSize.y + 1), new Vector3(-1, 0, -1));
        addConvexQuad(new Vector3(areaSize.x, 0, areaSize.y), new Vector3(0, 0, areaSize.y), new Vector3(areaSize.x + 1, 0, areaSize.y + 1), new Vector3(-1, 0, areaSize.y + 1));
        addConvexQuad(new Vector3(areaSize.x, 0, 0), new Vector3(areaSize.x, 0, areaSize.y), new Vector3(areaSize.x + 1, 0, -1), new Vector3(areaSize.x + 1, 0, areaSize.y + 1));
        addConvexQuad(new Vector3(0, 0, 0), new Vector3(areaSize.x, 0, 0), new Vector3(-1, 0, -1), new Vector3(areaSize.x + 1, 0, -1));
        //Floor Down
        //CreateConvexQuad(new Vector3(areaSize.x + 1, 0, -1), new Vector3(-1, 0, -1), new Vector3(areaSize.x + 1, 0, areaSize.y + 1), new Vector3(-1, 0, areaSize.y + 1));

        endGeneratingMesh();
    }

    // User Functions
    public void GenerateNewArena(ref int xSize, ref int ySize)
    {
        xSize = xSize < 2 ? 2 : xSize;
        ySize = ySize < 2 ? 2 : ySize;

        areaSize = new Vector2(xSize, ySize);

        createArena();
    }
}