﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ObstacleGenerator : MeshGenerator
{
    [Header("Object References")]
    [SerializeField] Transform LineRendererSpawnHolder;
    [SerializeField] GameObject lineRendererPrefab;
    [Header("Script References")]
    [SerializeField] protected ObstaclePainter obstaclePainter;
    [SerializeField] GeneratorScene_UiManager uiManager;
    [Header("Editor Values")]
    [SerializeField] float obsatcleHeight = 1;

    protected List<ObstaclePointGroup> obstaclePointGroups;

    Vector2Int[] adderVec = { new Vector2Int(-1, 1), new Vector2Int(0, 1), new Vector2Int(1, 1), new Vector2Int(1, 0), new Vector2Int(1, -1), new Vector2Int(0, -1), new Vector2Int(-1, -1), new Vector2Int(-1, 0) };

    float calculationBreakTime = 0.05f;
    int calculationsPerFrame = 50;
    int calculationCount = 0;


    IEnumerator process()
    {
        beforeProcess(); 
        resetCalculatedData();

        yield return sortPaintedObstaclePoints();
        yield return setGeneralBorders();
        yield return setFreeNeighbours();
        yield return setBorderChains();
        yield return setBorderSkips();

        foreach (ObstaclePointGroup pointGroup in obstaclePointGroups) // check for wrong Points
        {
            foreach (ObstaclePoint point in pointGroup.AllPoints.getPointArray())
            {
                if (point != null && point.type == ObstaclePointType.Invalid)
                {
                    uiManager.ActPlayMode = PlayMode.Editing;
                    yield break;
                }
            }
        }

        yield return createObstacleLineRenderers();
        yield return setBorderMeshLines();

        startGeneratingMesh("Obsatcles");
        yield return generateMeshWalls();
        yield return generateRoof();
        endGeneratingMesh();

        afterProcess();
    }

    protected virtual void beforeProcess()
    {
        uiManager.ActPlayMode = PlayMode.Generating;
    }

    protected virtual void afterProcess()
    {
        uiManager.ActPlayMode = PlayMode.Playing;
    }

    #region Step 1: Sort Points into seperated Obstacles

    IEnumerator sortPaintedObstaclePoints()
    {
        ObstaclePointRaster paintedPointsCopy = new ObstaclePointRaster(obstaclePainter.PaintedPoints.getPointList());

        while (paintedPointsCopy.getPointList().Count > 0)
        {
            List<ObstaclePoint> tempAllPoints = new List<ObstaclePoint>();
            List<ObstaclePoint> pointsToFindNeignors = new List<ObstaclePoint>();
            pointsToFindNeignors.Add(paintedPointsCopy.getPointList()[0]);

            while (pointsToFindNeignors.Count > 0)
            {
                ObstaclePoint actObs = pointsToFindNeignors[0];
                tempAllPoints.Add(actObs);
                pointsToFindNeignors.Remove(actObs);
                paintedPointsCopy.removePoint(actObs.rasterPos);

                List<ObstaclePoint> neighbors = getDirectNeighbours(actObs, paintedPointsCopy);

                foreach (ObstaclePoint tempNei in neighbors)
                {
                    pointsToFindNeignors.Add(tempNei);
                    paintedPointsCopy.removePoint(tempNei.rasterPos);

                    if (takeCalculationBreak()) yield return new WaitForSeconds(calculationBreakTime);
                }

                actObs.placeHolder.GetComponent<Renderer>().material.SetColor("_BaseColor", randomObstacleColor(obstaclePointGroups.Count));
            }

            ObstaclePointGroup newObstacleGroup = new ObstaclePointGroup();
            newObstacleGroup.AllPoints = new ObstaclePointRaster(tempAllPoints);
            newObstacleGroup.BorderPoints = new ObstaclePointRaster(newObstacleGroup.AllPoints.OriginPosition, newObstacleGroup.AllPoints.arraySize);
            newObstacleGroup.FreeNeighborPositions = new List<Vector2Int>();
            newObstacleGroup.BorderMeshLines = new List<List<Vector2Int>>();

            obstaclePointGroups.Add(newObstacleGroup);
        }
        yield return null;
    }

    List<ObstaclePoint> getDirectNeighbours(ObstaclePoint actObs, ObstaclePointRaster pointRaster)
    {
        // Gets the Top, Bottom, Left, Right Neighbor
        List<ObstaclePoint> back = new List<ObstaclePoint>();

        for (int x = -1; x <= 1; x += 2)
        {
            ObstaclePoint temp = pointRaster.findPoint(actObs.rasterPos.x + x, actObs.rasterPos.y);

            if (temp != null)
            {
                back.Add(temp);
            }
        }

        for (int y = -1; y <= 1; y += 2)
        {
            ObstaclePoint temp = pointRaster.findPoint(actObs.rasterPos.x, actObs.rasterPos.y + y);

            if (temp != null)
            {
                back.Add(temp);
            }
        }

        return back;
    }

    #endregion


    #region Step 2: Set the Obstacle Border and Field Neighbors

    IEnumerator setGeneralBorders()
    {
        foreach (ObstaclePointGroup pointGroup in obstaclePointGroups)
        {
            foreach (ObstaclePoint point in pointGroup.AllPoints.getPointList())
            {
                int neighborCount = setNeighborPoints(point, pointGroup.AllPoints).Count;

                if (neighborCount <= 1)
                {
                    point.type = ObstaclePointType.Invalid;
                }
                else if (neighborCount == 2)
                {
                    if (hasSpecificNeighbors(point, 0, 4) || hasSpecificNeighbors(point, 1, 5) || hasSpecificNeighbors(point, 2, 6) || hasSpecificNeighbors(point, 3, 7))
                        point.type = ObstaclePointType.Invalid;
                    else
                    {
                        point.type = ObstaclePointType.Border;
                        pointGroup.BorderPoints.addPoint(point);
                    }
                }
                else if (neighborCount == 8)
                {
                    point.type = ObstaclePointType.Inside;
                }
                else
                {
                    point.type = ObstaclePointType.Border;
                    pointGroup.BorderPoints.addPoint(point);
                }

                colorPlaceHolder(point);
                if (takeCalculationBreak()) yield return new WaitForSeconds(calculationBreakTime);
            }
        }
        yield return null;
    }

    List<ObstaclePoint> setNeighborPoints(ObstaclePoint actObs, ObstaclePointRaster pointRaster)
    {
        List<ObstaclePoint> back = new List<ObstaclePoint>();
        actObs.Neighbors = new ObstaclePoint[8];
        //get all 8 Neighbors of the field
        int counter = 0;

        for (int i = 0; i < 8; i++)
        {
            ObstaclePoint tempPoint = pointRaster.findPoint(actObs.rasterPos + adderVec[i]);
            actObs.Neighbors[counter++] = tempPoint;
            if (tempPoint != null) back.Add(tempPoint);
        }

        return back;
    }

    bool hasSpecificNeighbors(ObstaclePoint point, int field1, int field2, ObstaclePointType searchType = ObstaclePointType.Undetermined)
    {
        if (point.Neighbors[field1] == null || point.Neighbors[field2] == null) return false;
        if (searchType == ObstaclePointType.Undetermined && point.Neighbors[field1] != null && point.Neighbors[field2] != null) return true;

        if (point.Neighbors[field1].type == searchType && point.Neighbors[field2].type == searchType)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #endregion


    #region Step 3: Set Border Chain

    IEnumerator setFreeNeighbours()
    {
        foreach (ObstaclePointGroup pointGroup in obstaclePointGroups)
        {
            foreach (ObstaclePoint point in pointGroup.BorderPoints.getPointList())
            {
                for (int i = 0; i < 8; i++)
                {
                    Vector2Int tempSearchPos = point.rasterPos + adderVec[i];

                    if (pointGroup.AllPoints.findPoint(tempSearchPos) == null)
                    {
                        int borNeiOfSearchPos = 0;
                        for (int a = 0; a < 8; a++)
                        {
                            if (pointGroup.BorderPoints.findPoint(tempSearchPos + adderVec[a]) != null) borNeiOfSearchPos++;
                        }
                        if (borNeiOfSearchPos > 1)
                        {
                            pointGroup.FreeNeighborPositions.Add(point.rasterPos + adderVec[i]);
                        }
                    }

                    if (takeCalculationBreak(false)) yield return new WaitForSeconds(calculationBreakTime);
                }
            }
        }
        yield return null;
    }

    IEnumerator setBorderChains()
    {
        foreach (ObstaclePointGroup pointGroup in obstaclePointGroups)
        {
            foreach (Vector2Int freePos in pointGroup.FreeNeighborPositions)
            {
                int tempAdderPos = 1;
                Again:
                if (freePosIsPeakNeighbor(freePos, pointGroup.BorderPoints, ref tempAdderPos))
                {
                    if (takeCalculationBreak(false)) yield return new WaitForSeconds(calculationBreakTime);

                    ObstaclePoint tempPoint1 = pointGroup.BorderPoints.findPoint(freePos + adderVec[tempAdderPos]);
                    ObstaclePoint tempPoint2 = pointGroup.BorderPoints.findPoint(freePos + adderVec[(tempAdderPos + 2) % 8]);

                    if (tempPoint1.BorderFollower == null && tempPoint2.BorderLeader == null)
                    {
                        tempPoint1.BorderFollower = tempPoint2;
                        tempPoint2.BorderLeader = tempPoint1;
                    }

                    if ((tempAdderPos += 2) <= 7)
                    {
                        goto Again;
                    }
                }

                ObstaclePoint tempActPoint = null;
                ObstaclePoint tempFollower = null;
                for (int i = 0; i < 8; i++)
                {
                    tempActPoint = pointGroup.BorderPoints.findPoint(freePos + adderVec[i]);
                    tempFollower = pointGroup.BorderPoints.findPoint(freePos + adderVec[(i + 1) % 8]);

                    if (tempActPoint != null && tempFollower != null)
                    {
                        if (tempActPoint.BorderFollower == null && tempFollower.BorderLeader == null)
                        {
                            tempActPoint.BorderFollower = tempFollower;
                            tempFollower.BorderLeader = tempActPoint;
                        }
                    }

                    if (takeCalculationBreak(false)) yield return new WaitForSeconds(calculationBreakTime);
                }
            }

            List<ObstaclePoint> toRemove = new List<ObstaclePoint>();
            foreach (ObstaclePoint actPoint in pointGroup.BorderPoints.getPointList())
            {
                if (actPoint.BorderLeader != null && actPoint.BorderFollower != null)
                {
                    actPoint.type = ObstaclePointType.BorderChained;
                }
                else if (actPoint.BorderLeader == null && actPoint.BorderFollower == null)
                {
                    actPoint.type = ObstaclePointType.BorderSkip;
                    pointGroup.BorderPoints.removePoint(actPoint.rasterPos);
                }
                else
                {
                    actPoint.type = ObstaclePointType.Invalid;
                    pointGroup.BorderPoints.removePoint(actPoint.rasterPos);
                }

                colorPlaceHolder(actPoint);
                if (takeCalculationBreak()) yield return new WaitForSeconds(calculationBreakTime);
            }
        }

        yield return null;
    }

    bool freePosIsPeakNeighbor(Vector2Int freePos, ObstaclePointRaster borderGroup, ref int a)
    {
        bool back = false;

        for (/*1*/; a < 8; a += 2)
        {
            if ((borderGroup.findPoint(freePos + adderVec[(a - 1) % 8]) == null || borderGroup.findPoint(freePos + adderVec[(a + 3) % 8]) == null))
            {
                for (int b = 0; b < 3; b++)
                {
                    if (borderGroup.findPoint(freePos + adderVec[(a + b) % 8]) == null)
                    {
                        goto Loser;
                    }
                }

                back = true;
                break;
            }

            Loser:;
        }

        return back;
    }

    IEnumerator setBorderSkips()
    {
        foreach (ObstaclePointGroup pointGroup in obstaclePointGroups)
        {
            foreach (ObstaclePoint point in pointGroup.BorderPoints.getPointList())
            {
                if (point.type != ObstaclePointType.BorderChained)
                {
                    Debug.LogWarning("There is an wrong field in the Border-List");
                    continue;
                }

                if (hasStraightBorderNeighbors(point))
                {
                    point.type = ObstaclePointType.BorderSkip;
                    point.BorderFollower.BorderLeader = point.BorderLeader;
                    point.BorderLeader.BorderFollower = point.BorderFollower;
                    pointGroup.BorderPoints.removePoint(point.rasterPos);
                }
                else
                {
                    point.type = ObstaclePointType.Border;
                }

                colorPlaceHolder(point);
                if (takeCalculationBreak()) yield return new WaitForSeconds(calculationBreakTime);
            }
        }
        yield return null;
    }

    bool hasStraightBorderNeighbors(ObstaclePoint point)
    {
        Vector2 delta1 = point.BorderLeader.rasterPos - point.rasterPos;
        Vector2 delta2 = point.rasterPos - point.BorderFollower.rasterPos;

        return (delta1.normalized == delta2.normalized);
    }


    #endregion


    #region Step 4: Create the Mesh Walls

    IEnumerator createObstacleLineRenderers()
    {
        foreach (ObstaclePointGroup pointGroup in obstaclePointGroups)
        {
            List<ObstaclePoint> borderPointsCopy = pointGroup.BorderPoints.getPointList();

            ObstaclePoint actPoint = borderPointsCopy[0];
            while (borderPointsCopy.Count != 0)
            {
                GameObject lineRendererObject = Instantiate(lineRendererPrefab, new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0));
                lineRendererObject.transform.parent = LineRendererSpawnHolder;
                LineRenderer lineRender = lineRendererObject.GetComponent<LineRenderer>();
                lineRender.positionCount = 0;

                BorderLineDirection direction = getLineDirection(actPoint, actPoint.BorderFollower, pointGroup);

                ObstaclePoint firstPoint = actPoint;
                while (firstPoint != actPoint || lineRender.positionCount == 0)
                {
                    lineRender.positionCount++;
                    lineRender.SetPosition(lineRender.positionCount - 1, new Vector3(actPoint.rasterPos.x, obsatcleHeight + 0.5f, actPoint.rasterPos.y));

                    borderPointsCopy.Remove(actPoint);

                    if (direction == BorderLineDirection.ToFollower)
                        actPoint = actPoint.BorderFollower;
                    else
                        actPoint = actPoint.BorderLeader;

                    if (takeCalculationBreak()) yield return new WaitForSeconds(calculationBreakTime);
                }
                lineRender.positionCount++;
                lineRender.SetPosition(lineRender.positionCount - 1, new Vector3(firstPoint.rasterPos.x, obsatcleHeight + 0.5f, firstPoint.rasterPos.y));

                if (borderPointsCopy.Count > 1)
                {
                    actPoint = borderPointsCopy[0];
                }
            }
        }
        yield return null;
    }

    IEnumerator setBorderMeshLines()
    {
        foreach (ObstaclePointGroup pointGroup in obstaclePointGroups)
        {
            List<ObstaclePoint> borderPointsCopy = pointGroup.BorderPoints.getPointList();
            ObstaclePoint actPoint = borderPointsCopy[0];

            while (borderPointsCopy.Count != 0)
            {
                List<Vector2Int> borderLine = new List<Vector2Int>();
                BorderLineDirection direction = getLineDirection(actPoint, actPoint.BorderFollower, pointGroup);

                ObstaclePoint firstPoint = actPoint;
                while (firstPoint != actPoint || borderLine.Count == 0)
                {
                    borderLine.Add(actPoint.rasterPos);

                    borderPointsCopy.Remove(actPoint);

                    if (direction == BorderLineDirection.ToFollower)
                        actPoint = actPoint.BorderFollower;
                    else
                        actPoint = actPoint.BorderLeader;


                    if (takeCalculationBreak()) yield return new WaitForSeconds(calculationBreakTime);
                }
                borderLine.Add(firstPoint.rasterPos);

                pointGroup.BorderMeshLines.Add(borderLine);

                if (borderPointsCopy.Count > 1)
                {
                    actPoint = borderPointsCopy[0];
                }
            }
        }
        yield return null;
    }

    BorderLineDirection getLineDirection(ObstaclePoint leaderPoint, ObstaclePoint followerPoint, ObstaclePointGroup pointGroup)
    {
        BorderLineDirection back = BorderLineDirection.Error;

        Vector2 delta = followerPoint.rasterPos - leaderPoint.rasterPos;
        delta.x = delta.x < -1 ? -1 : delta.x;
        delta.x = delta.x > 1 ? 1 : delta.x;
        delta.y = delta.y < -1 ? -1 : delta.y;
        delta.y = delta.y > 1 ? 1 : delta.y;

        int adderNumber = 0;
        for (int i = 0; i < 8; i++)
        {
            if (delta == adderVec[i])
            {
                adderNumber = i;
                break;
            }
        }

        Vector2Int rightPoint = leaderPoint.rasterPos + adderVec[(adderNumber + 1) % 8];
        Vector2Int leftPoint = leaderPoint.rasterPos + adderVec[(adderNumber + 7) % 8];

        if (pointGroup.AllPoints.findPoint(rightPoint) != null)
        {
            back = BorderLineDirection.ToLeader;
        }
        else if (pointGroup.AllPoints.findPoint(leftPoint) != null)
        {
            back = BorderLineDirection.ToFollower;
        }
        else if (pointGroup.AllPoints.findPoint(leftPoint) != null && pointGroup.AllPoints.findPoint(rightPoint) != null)
        {
            back = BorderLineDirection.Error;
            leaderPoint.type = ObstaclePointType.Invalid;
            colorPlaceHolder(leaderPoint);
            Debug.LogError("Developer made an mistake while calculating the direction");
        }

        return back;
    }

    IEnumerator generateMeshWalls()
    {
        foreach (ObstaclePointGroup pointGroup in obstaclePointGroups)
        {
            foreach (List<Vector2Int> borderLine in pointGroup.BorderMeshLines)
            {
                for (int i = 0; i < borderLine.Count - 1; i++)
                {
                    Vector3 DL = My_Math.ToVector3D(borderLine[i]);
                    Vector3 DR = My_Math.ToVector3D(borderLine[i + 1]);
                    Vector3 UL = DL + new Vector3(0, obsatcleHeight, 0);
                    Vector3 UR = DR + new Vector3(0, obsatcleHeight, 0);

                    addConvexQuad(DL, DR, UL, UR);

                    if (takeCalculationBreak()) yield return new WaitForSeconds(calculationBreakTime);
                }
            }
        }
        yield return null;
    }

    #endregion


    #region Step 5: Create the Mesh Roofs

    IEnumerator generateRoof()
    {
        foreach (ObstaclePointGroup pointGroup in obstaclePointGroups)
        {
            RoofForm[,] oldRoof = getRoofFormArray(pointGroup, out int worldX, out int worldY);
            yield return null;
            RoofForm[] newRoof = getReducedFormArray(oldRoof, new Vector2Int(worldX, worldY));
            yield return null;
            generateRoofMesh(newRoof);
            yield return null;
        }
    }

    RoofForm[,] getRoofFormArray(ObstaclePointGroup pointGroup, out int worldX, out int worldY)
    {
        int minX = pointGroup.BorderPoints.OriginPosition.x;
        int maxX = pointGroup.BorderPoints.MaxPosition.x;
        int minY = pointGroup.BorderPoints.OriginPosition.y;
        int maxY = pointGroup.BorderPoints.MaxPosition.y;

        RoofForm[,] roofFields = new RoofForm[maxX - minX, maxY - minY];

        for (int xPos = minX; xPos < maxX; xPos++)
        {
            for (int yPos = minY; yPos < maxY; yPos++)
            {
                ObstaclePoint[,] fieldPoints = new ObstaclePoint[2, 2];
                int existingPoints = 0;

                for (int xAdd = 0; xAdd <= 1; xAdd++)
                {
                    for (int yAdd = 0; yAdd <= 1; yAdd++)
                    {
                        fieldPoints[xAdd, yAdd] = pointGroup.AllPoints.findPoint(new Vector2Int(xPos + xAdd, yPos + yAdd));
                        if (fieldPoints[xAdd, yAdd] != null)
                        {
                            existingPoints++;
                        }
                    }
                }

                Vector2Int DL = new Vector2Int(xPos, yPos);
                Vector2Int DR = new Vector2Int(xPos + 1, yPos);
                Vector2Int UL = new Vector2Int(xPos, yPos + 1);
                Vector2Int UR = new Vector2Int(xPos + 1, yPos + 1);

                if (existingPoints == 4)
                {
                    roofFields[xPos - minX, yPos - minY] = new RoofSquare(DL, DR, UL, UR);
                }
                else if (existingPoints == 3)
                {
                    if (fieldPoints[0, 0] == null) // DL == null
                    {
                        bool test1 = pointGroup.AllPoints.findPoint(xPos - 1, yPos + 1) == null;
                        bool test2 = pointGroup.AllPoints.findPoint(xPos + 1, yPos - 1) == null;

                        if (test1 || test2)
                        {
                            roofFields[xPos - minX, yPos - minY] = new RoofTriangle(UL, UR, DR, RoofTriangleType.UpRight);
                        }
                    }
                    else if (fieldPoints[1, 0] == null)
                    {
                        bool test1 = pointGroup.AllPoints.findPoint(xPos + 2, yPos + 1) == null;
                        bool test2 = pointGroup.AllPoints.findPoint(xPos, yPos - 1) == null;

                        if (test1 || test2)
                        {
                            roofFields[xPos - minX, yPos - minY] = new RoofTriangle(DL, UL, UR, RoofTriangleType.UpLeft);
                        }
                    }
                    else if (fieldPoints[0, 1] == null)
                    {
                        bool test1 = pointGroup.AllPoints.findPoint(xPos + 1, yPos + 2) == null;
                        bool test2 = pointGroup.AllPoints.findPoint(xPos - 1, yPos) == null;

                        if (test1 || test2)
                        {
                            roofFields[xPos - minX, yPos - minY] = new RoofTriangle(DL, UR, DR, RoofTriangleType.DownRight);
                        }
                    }
                    else if (fieldPoints[1, 1] == null)
                    {
                        bool test1 = pointGroup.AllPoints.findPoint(xPos + 2, yPos) == null;
                        bool test2 = pointGroup.AllPoints.findPoint(xPos, yPos + 2) == null;

                        if (test1 || test2)
                        {
                            roofFields[xPos - minX, yPos - minY] = new RoofTriangle(UL, DR, DL, RoofTriangleType.DownLeft);
                        }
                    }
                }
            }
        }

        worldX = minX;
        worldY = minY;
        return roofFields;
    }

    RoofForm[] getReducedFormArray(RoofForm[,] ogRoofForms, Vector2Int worldPos)
    {
        List<RoofForm> finalRoofForms = new List<RoofForm>();

        int arraySizeX = ogRoofForms.GetLength(0);
        int arraySizeY = ogRoofForms.GetLength(1);

        // extend Triangles
        for (int xPos = 0; xPos < arraySizeX; xPos++)
        {
            for (int yPos = 0; yPos < arraySizeY; yPos++)
            {
                if (ogRoofForms[xPos, yPos] == null || ogRoofForms[xPos, yPos].GetType() != typeof(RoofTriangle)) continue;

                RoofTriangle actTri = ogRoofForms[xPos, yPos] as RoofTriangle;
                ogRoofForms[xPos, yPos] = null;

                int addedLines = 1;
                int xToCheck = xPos + addedLines;
                int yToCheck = yPos + addedLines * actTri.extendYAdder;

                while (xToCheck < arraySizeX && yToCheck < arraySizeY && yToCheck >= 0)
                {
                    var tempFrom = ogRoofForms[xToCheck, yToCheck];
                    if (tempFrom == null || tempFrom.GetType() != typeof(RoofTriangle) || (tempFrom as RoofTriangle).TriType != actTri.TriType) goto LineAddingIsFinished;

                    switch (actTri.TriType)
                    {
                        case RoofTriangleType.UpLeft:
                            for (int i = 0; i < addedLines; i++)
                            {
                                var tempForm = ogRoofForms[xPos + i, yToCheck];
                                if (tempForm == null || tempForm.GetType() != typeof(RoofSquare)) goto LineAddingIsFinished;
                            }
                            for (int i = 0; i < addedLines; i++) ogRoofForms[xPos + i, yToCheck] = null;
                            break;

                        case RoofTriangleType.UpRight:
                            for (int i = 0; i > -addedLines; i--)
                            {
                                var tempForm = ogRoofForms[xToCheck, yPos + i];
                                if (tempForm == null || tempForm.GetType() != typeof(RoofSquare)) goto LineAddingIsFinished;
                            }
                            for (int i = 0; i > -addedLines; i--) ogRoofForms[xToCheck, yPos + i] = null;
                            break;

                        case RoofTriangleType.DownLeft:
                            for (int i = 0; i < addedLines; i++)
                            {
                                var tempForm = ogRoofForms[xPos + i, yToCheck];
                                if (tempForm == null || tempForm.GetType() != typeof(RoofSquare)) goto LineAddingIsFinished;
                            }
                            for (int i = 0; i < addedLines; i++) ogRoofForms[xPos + i, yToCheck] = null;
                            break;

                        case RoofTriangleType.DownRight:
                            for (int i = 0; i < addedLines; i++)
                            {
                                var tempForm = ogRoofForms[xToCheck, yPos + i];
                                if (tempForm == null || tempForm.GetType() != typeof(RoofSquare)) goto LineAddingIsFinished;
                            }
                            for (int i = 0; i < addedLines; i++) ogRoofForms[xToCheck, yPos + i] = null;
                            break;
                    }

                    ogRoofForms[xToCheck, yToCheck] = null;

                    addedLines++;
                    xToCheck = xPos + addedLines;
                    yToCheck = yPos + addedLines * actTri.extendYAdder;
                }

                LineAddingIsFinished:
                addedLines--;

                if (addedLines > 0)
                {
                    switch (actTri.TriType)
                    {
                        case RoofTriangleType.UpLeft:
                            actTri.points[0] = worldPos + new Vector2Int(xPos, yPos);
                            actTri.points[1] = worldPos + new Vector2Int(xPos, yPos + addedLines + 1);
                            actTri.points[2] = worldPos + new Vector2Int(xPos + addedLines + 1, yPos + addedLines + 1);
                            break;
                        case RoofTriangleType.UpRight:
                            actTri.points[0] = worldPos + new Vector2Int(xPos, yPos + 1);
                            actTri.points[1] = worldPos + new Vector2Int(xPos + addedLines + 1, yPos + 1);
                            actTri.points[2] = worldPos + new Vector2Int(xPos + addedLines + 1, yPos - addedLines);
                            break;
                        case RoofTriangleType.DownLeft:
                            actTri.points[0] = worldPos + new Vector2Int(xPos, yPos + 1);
                            actTri.points[1] = worldPos + new Vector2Int(xPos + addedLines + 1, yPos - addedLines);
                            actTri.points[2] = worldPos + new Vector2Int(xPos, yPos - addedLines);
                            break;
                        case RoofTriangleType.DownRight:
                            actTri.points[0] = worldPos + new Vector2Int(xPos, yPos);
                            actTri.points[1] = worldPos + new Vector2Int(xPos + addedLines + 1, yPos + addedLines + 1);
                            actTri.points[2] = worldPos + new Vector2Int(xPos + addedLines + 1, yPos);
                            break;
                    }
                }

                finalRoofForms.Add(actTri);
            }
        }


        // expend Squares
        for (int xPos = 0; xPos < arraySizeX; xPos++)
        {
            for (int yPos = 0; yPos < arraySizeY; yPos++)
            {
                if (ogRoofForms[xPos, yPos] == null || ogRoofForms[xPos, yPos].GetType() != typeof(RoofSquare)) continue;

                RoofSquare actSquare = ogRoofForms[xPos, yPos] as RoofSquare;
                ogRoofForms[xPos, yPos] = null;

                int addedFields = 1;
                int xAdder = 0;
                int yAdder = 0;
                bool canExtendX = true;
                bool canExtendY = true;

                while (canExtendX || canExtendY)
                {
                    if (canExtendX)
                    {
                        xAdder++;
                        if (xPos + xAdder > arraySizeX - 1)
                        {
                            canExtendX = false;
                            xAdder--;
                        }
                        else
                        {
                            for (int vert = 0; vert < addedFields; vert++)
                            {
                                var tempForm = ogRoofForms[xPos + xAdder, yPos + vert];
                                if (tempForm == null || tempForm.GetType() != typeof(RoofSquare))
                                {
                                    canExtendX = false;
                                    xAdder--;
                                    break;
                                }
                            }
                        }

                        if (canExtendX && canExtendY) addedFields++;
                    }

                    if (canExtendY)
                    {
                        yAdder++;
                        if (yPos + yAdder > arraySizeY - 1)
                        {
                            canExtendY = false;
                            yAdder--;
                            addedFields--;
                        }
                        else
                        {
                            for (int hori = 0; hori < addedFields; hori++)
                            {
                                var tempForm = ogRoofForms[xPos + hori, yPos + yAdder];
                                if (tempForm == null || tempForm.GetType() != typeof(RoofSquare))
                                {
                                    canExtendY = false;
                                    yAdder--;
                                    addedFields--;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (xAdder > 0 || yAdder > 0)
                {
                    actSquare.points[1] += new Vector2Int(xAdder, 0);
                    actSquare.points[2] += new Vector2Int(0, yAdder);
                    actSquare.points[3] += new Vector2Int(xAdder, yAdder);

                    for (int x = 0; x <= xAdder; x++)
                    {
                        for (int y = 0; y <= yAdder; y++)
                        {
                            ogRoofForms[xPos + x, yPos + y] = null;
                        }
                    }
                }

                finalRoofForms.Add(actSquare);

                //Debug.Log("Square = " + actSquare.points[0] + " " + actSquare.points[1] + " " + actSquare.points[2] + " " + actSquare.points[3]);
            }
        }

        return finalRoofForms.ToArray();
    }

    void generateRoofMesh(RoofForm[] roofForms)
    {
        foreach (RoofForm form in roofForms)
        {
            if (form.GetType() == typeof(RoofSquare))
            {
                RoofSquare tempSquare = form as RoofSquare;
                Vector3 DL = new Vector3(tempSquare.points[0].x, obsatcleHeight, tempSquare.points[0].y);
                Vector3 DR = new Vector3(tempSquare.points[1].x, obsatcleHeight, tempSquare.points[1].y);
                Vector3 UL = new Vector3(tempSquare.points[2].x, obsatcleHeight, tempSquare.points[2].y);
                Vector3 UR = new Vector3(tempSquare.points[3].x, obsatcleHeight, tempSquare.points[3].y);

                addConvexQuad(DL, DR, UL, UR);
            }
            else if (form.GetType() == typeof(RoofTriangle))
            {
                RoofTriangle tempTri = form as RoofTriangle;
                Vector3 P1 = new Vector3(tempTri.points[0].x, obsatcleHeight, tempTri.points[0].y);
                Vector3 P2 = new Vector3(tempTri.points[1].x, obsatcleHeight, tempTri.points[1].y);
                Vector3 P3 = new Vector3(tempTri.points[2].x, obsatcleHeight, tempTri.points[2].y);

                addTriangle(P1, P2, P3);
            }
        }
    }

    #endregion


    #region Start, End, Clear and Calculation

    bool takeCalculationBreak(bool visualisationExits = true)
    {
        if (calculationBreakTime > 0 && visualisationExits)
        {
            return true;
        }
        else if (++calculationCount >= calculationsPerFrame)
        {
            calculationCount = 0;
            return true;
        }
        return false;
    }

    void resetCalculatedData()
    {
        DeleteAllObstacles();

        FindObjectOfType<ObstaclePainter>().PaintedPoints.resetAllPoints();
        obstaclePointGroups = new List<ObstaclePointGroup>();
    }

    /// <summary>
    ///  Deletes all obsatcles / data that are generated in ObsatcleGenerator. The ObstaclePoints / data in ObsatclePainter remain.
    /// </summary>
    public void DeleteAllObstacles()
    {
        if (obstaclePointGroups != null) obstaclePointGroups.Clear();

        LineRenderer[] oldLines = FindObjectsOfType<LineRenderer>();
        for (int i = 0; i < oldLines.Length; i++)
        {
            Destroy(oldLines[i].gameObject);
        }

        if (mesh != null)
        {
            mesh.Clear();
            GetComponent<MeshFilter>().mesh = null;
            GetComponent<MeshCollider>().sharedMesh = null;
        }
    }

    public void StartGeneratingObsatcles()
    {
        StartCoroutine(process());
    }

    public void setBreakTime(float time)
    {
        if (time < 0) time = 0;
        calculationBreakTime = time;
    }

    public float ObstacleHeight
    {
        get { return obsatcleHeight; }
        set { obsatcleHeight = value > 1 ? value : 1; }
    }

    public void showObsatcleLines(bool showLines)
    {
        LineRendererSpawnHolder.gameObject.SetActive(showLines);
    }

    #endregion


    //Color Functions
    Color randomObstacleColor(int number)
    {
        Color[] colors = { new Color(1, 1, 0.4f), new Color(0.6f, 1, 0.6f), new Color(1, 0.7f, 0.7f), new Color(0.8f, 0.8f, 1), new Color(0.8f, 1, 1) };
        return colors[number % 5];
    }

    void colorPlaceHolder(ObstaclePoint point)
    {
        switch (point.type)
        {
            case ObstaclePointType.Border:
                point.placeHolder.GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(0, 0.15f, 1));
                break;
            case ObstaclePointType.BorderChained:
                point.placeHolder.GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(0.6f, 0.8f, 1));
                break;
            case ObstaclePointType.BorderSkip:
                goto case ObstaclePointType.Inside;
            case ObstaclePointType.Inside:
                point.placeHolder.GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(0, 0.5f, 1));
                break;
            case ObstaclePointType.Undetermined:
                point.placeHolder.GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(0.2f, 0.2f, 0.2f));
                break;
            case ObstaclePointType.Invalid:
                point.placeHolder.GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(1, 0, 0));
                break;
            default:
                break;
        }
    }


    // Structs and Enums

    enum BorderLineDirection
    {
        ToLeader, ToFollower, Error
    }
}

public struct ObstaclePointGroup
{
    public ObstaclePointRaster AllPoints;
    public ObstaclePointRaster BorderPoints;
    public List<List<Vector2Int>> BorderMeshLines;
    public List<Vector2Int> FreeNeighborPositions;
}
