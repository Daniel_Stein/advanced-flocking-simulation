﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeneratorScene_UiManager : UiManagerBase
{
    [Header("Scene UI References")]
    [SerializeField] InputField arenaSizeX_InputField;
    [SerializeField] InputField arenaSizeY_InputField;
    [SerializeField] Button arenaSize_Button;
    [SerializeField] InputField brushSize_InputField;
    [SerializeField] Toggle roundBrush_Toggle;
    [SerializeField] Toggle squareBrush_Toggle;
    [SerializeField] Text obstacleCreateEditButton_Text;
    [SerializeField] Button obstacleCreateEdit_Button;
    [SerializeField] Button obstacleClear_Button;
    [SerializeField] Slider obstacleHight_Slider;
    [SerializeField] Text obstacleHight_Text;
    [SerializeField] Slider generationTime_Slider;
    [SerializeField] Image[] generationTime_Images = new Image[2];
    [SerializeField] GameObject saveLoad_Panel;
    [SerializeField] GameObject saveMode_TogglePanel;
    [SerializeField] GameObject savePath_Panel;
    [SerializeField] InputField savePath_InputField;
    [SerializeField] GameObject loadMap_Panel;
    [SerializeField] RectTransform loadMapScrollContent;
    [SerializeField] GameObject loadMapEntrance_Prefab;

    [Header("Script Object References")]
    [SerializeField] BorderGenerator borderGenerator;
    [SerializeField] ObstaclePainter obstaclePainter;
    [SerializeField] ObstacleGenerator obstacleGenerator;
    [SerializeField] PlayerController playerController;
    [SerializeField] MapSaverLoader mapSaverLoader;


    void Start()
    {
        getPlayerPrefsAtStart();

    }

    void getPlayerPrefsAtStart()
    {
        int xSize = PlayerPrefs.GetInt("ArenaSizeX", 50);
        int ySize = PlayerPrefs.GetInt("ArenaSizeY", 50);
        arenaSizeX_InputField.text = "" + xSize;
        arenaSizeY_InputField.text = "" + ySize;
        CreateArena();

        int brushSize = PlayerPrefs.GetInt("BrushSize", 5);
        brushSize_InputField.text = "" + brushSize;
        SetBrushSize();

        if (PlayerPrefs.GetString("BrushMode", "Round") == "Round")
        {
            roundBrush_Toggle.isOn = true;
            SetBrushformToRound();
        }
        else
        {
            squareBrush_Toggle.isOn = true;
            SetBrushformToSquare();
        }

        float generationBreakTime = PlayerPrefs.GetFloat("GenerationBreakTime", 0.02f);
        generationTime_Slider.value = generationBreakTime;
        SetGenerationBreakTime(generationBreakTime);

        float obstacleHight = PlayerPrefs.GetFloat("ObstacleHeight", 1);
        obstacleHight_Slider.value = obstacleHight;
        SetObstacleHight(obstacleHight);

        ActPlayMode = PlayMode.Editing;

#if !UNITY_EDITOR
        saveMode_TogglePanel.SetActive(false);
        saveLoad_Panel.GetComponent<RectTransform>().sizeDelta -= new Vector2(0, 40);
        SetSaveModeToJson();
#endif

        playerController.isTypingStuff(false);
    }


    #region Set Data

    public void CreateArena()
    {
        int xInput, yInput;

        int.TryParse(arenaSizeX_InputField.text, out xInput);
        int.TryParse(arenaSizeY_InputField.text, out yInput);

        borderGenerator.GenerateNewArena(ref xInput, ref yInput);

        PlayerPrefs.SetInt("ArenaSizeX", xInput);
        PlayerPrefs.SetInt("ArenaSizeY", yInput);

        arenaSizeX_InputField.text = "" + xInput;
        arenaSizeY_InputField.text = "" + yInput;

        obstacleGenerator.DeleteAllObstacles();
        obstaclePainter.setObsatclePointArraySize();
    }

    public void SetBrushSize()
    {
        ushort size;
        ushort.TryParse(brushSize_InputField.text, out size);

        obstaclePainter.setBrushSize(ref size);
        PlayerPrefs.SetInt("BrushSize", size);

        brushSize_InputField.text = "" + size;
    }

    public void SetBrushformToRound()
    {
        obstaclePainter.setBrushForm(BrushForm.Round);
        PlayerPrefs.SetString("BrushMode", "Round");
    }

    public void SetBrushformToSquare()
    {
        obstaclePainter.setBrushForm(BrushForm.Square);
        PlayerPrefs.SetString("BrushMode", "Square");
    }

    public void GenerateEdit_AllObsatcles()
    {
        if (ActPlayMode == PlayMode.Playing)
        {
            ActPlayMode = PlayMode.Editing;
        }
        else if (ActPlayMode == PlayMode.Editing)
        {
            if (obstaclePainter.PaintedPoints.getPointList().Count != 0)
            {
                obstacleGenerator.StartGeneratingObsatcles();
            }
        }
    }

    public void DeleteAllObsatcles()
    {
        obstacleGenerator.DeleteAllObstacles();
        obstaclePainter.DeleteAllPoints();
    }

    public void SetGenerationBreakTime(float time)
    {
        if (time == 0)
        {
            generationTime_Images[0].color = new Color(0.46f, 0.93f, 0.78f);
            generationTime_Images[1].color = new Color(0.46f, 0.93f, 0.78f);
        }
        else
        {
            generationTime_Images[0].color = Color.white;
            generationTime_Images[1].color = Color.white;
        }

        obstacleGenerator.setBreakTime(time);
        PlayerPrefs.SetFloat("GenerationBreakTime", time);
    }

    public void SetObstacleHight(float hight)
    {
        obstacleGenerator.ObstacleHeight = hight;
        obstacleHight_Text.text = "" + hight;

        PlayerPrefs.SetFloat("ObstacleHeight", hight);
    }

    #endregion


    #region Save and Load Fuctions
    public void SetSaveModeToJson()
    {
        mapSaverLoader.setSaveMode(MapSaverLoader.MapSaveMode.Json);
    }

    public void SetSaveModeToUnity()
    {
        mapSaverLoader.setSaveMode(MapSaverLoader.MapSaveMode.InsideUnity);
    }

    public void SaveMap()
    {
        closePanel(loadMap_Panel);
        openPanel(savePath_Panel);
        savePath_InputField.text = mapSaverLoader.saveMap();
    }

    public void CopyToClipboard(InputField UiText)
    {
        GUIUtility.systemCopyBuffer = UiText.text;
    }


    public void CreateLoadList()
    {
        MapSaverLoader.SavedMapFiles[] savedFiles = MapSaverLoader.getSavedMapList();

        closePanel(savePath_Panel);
        openPanel(loadMap_Panel);
        loadMapScrollContent.sizeDelta = new Vector2(0, savedFiles.Length * 40 + 10);

        for (int i = 1; i < loadMapScrollContent.childCount; i++)
        {
            Destroy(loadMapScrollContent.GetChild(i).gameObject);
        }

        foreach (var file in savedFiles)
        {
            GameObject temp = Instantiate(loadMapEntrance_Prefab, loadMapScrollContent);
            temp.GetComponentInChildren<Text>().text = file.mapName;
            temp.SetActive(true);
        }
    }

    public void loadMap(Text MapName_Text)
    {
        mapSaverLoader.loadMap(MapName_Text.text, out Vector2 areaSize, out List<Vector2Int> paintedPoints, out float obstacleHeight);

        SetObstacleHight(obstacleHeight);

        arenaSizeX_InputField.text = "" + areaSize.x;
        arenaSizeY_InputField.text = "" + areaSize.y;
        CreateArena();

        DeleteAllObsatcles();

        foreach (Vector2Int point in paintedPoints)
        {
            obstaclePainter.paintSingleObsatclePoint(PaintTool.Draw, point);
        }

        ActPlayMode = PlayMode.Editing;
        playerController.isTypingStuff(false);
    }

    #endregion


    // GameStates
    public override PlayMode ActPlayMode
    {
        get { return actPlayMode; }
        set
        {
            actPlayMode = value;

            switch (value)
            {
                case PlayMode.Playing:
                    obstacleCreateEdit_Button.interactable = true;
                    obstacleCreateEditButton_Text.text = "Edit";
                    obstaclePainter.showObsatclePoints(false);
                    break;

                case PlayMode.Editing:
                    arenaSize_Button.interactable = true;
                    obstacleClear_Button.interactable = true;
                    obstacleCreateEdit_Button.interactable = true;
                    obstacleHight_Slider.interactable = true;
                    obstacleCreateEditButton_Text.text = "Generate";
                    obstacleGenerator.DeleteAllObstacles();
                    obstaclePainter.showObsatclePoints(true);
                    break;

                case PlayMode.Generating:
                    arenaSize_Button.interactable = false;
                    obstacleClear_Button.interactable = false;
                    obstacleCreateEdit_Button.interactable = false;
                    obstacleHight_Slider.interactable = false;
                    obstacleCreateEditButton_Text.text = "...";

                    break;
                default:
                    break;
            }
        }
    }

}
