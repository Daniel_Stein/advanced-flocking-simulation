﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider))]
public class MeshGenerator : MonoBehaviour
{
    protected Mesh mesh;
    List<Vector3> verticies = new List<Vector3>();
    List<int> triangles = new List<int>();

    protected void startGeneratingMesh(string MeshName)
    {
        mesh = new Mesh();
        verticies = new List<Vector3>();
        triangles = new List<int>();
        mesh.name = MeshName;
    }

    protected void endGeneratingMesh()
    {
        mesh.Clear();
        mesh.vertices = verticies.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();

        GetComponent<MeshFilter>().mesh = mesh;
        GetComponent<MeshCollider>().sharedMesh = mesh;
    }

    protected void addConvexQuad(Vector3 DL, Vector3 DR, Vector3 UL, Vector3 UR)
    {
        verticies.Add(DL);
        verticies.Add(DR);
        verticies.Add(UL);
        verticies.Add(UR);

        int vCount = verticies.Count;
        int[] temp =
        {
            vCount-4, vCount-1, vCount-3,
            vCount-4, vCount-2, vCount-1
        };
        triangles.AddRange(temp);
    }

    protected void addTriangle(Vector3 P1, Vector3 P2, Vector3 P3)
    {
        verticies.Add(P1);
        verticies.Add(P2);
        verticies.Add(P3);

        int vCount = verticies.Count;
        int[] temp =
        {
            vCount-3, vCount-2, vCount-1,
        };
        triangles.AddRange(temp);
    }
}