﻿using UnityEngine;
using System.Collections;
using System.Security.Policy;
using System;

public abstract class RoofForm
{
    public Vector2Int[] points;
}

public class RoofSquare : RoofForm
{
    public RoofSquare(Vector2Int DL, Vector2Int DR, Vector2Int UL, Vector2Int UR)
    {
        points = new Vector2Int[4] { DL, DR, UL, UR };
    }
}

public class RoofTriangle : RoofForm
{
    RoofTriangleType triType = RoofTriangleType.Undetermined;

    public int extendYAdder = 0;
    public bool extendTriIsAtFront;

    public RoofTriangle(Vector2Int P1, Vector2Int P2, Vector2Int P3, RoofTriangleType Type)
    {
        points = new Vector2Int[3] { P1, P2, P3 };
        triType = Type;

        switch (triType)
        {
            case RoofTriangleType.UpLeft:
                extendYAdder = 1;
                extendTriIsAtFront = false;
                break;
            case RoofTriangleType.UpRight:
                extendYAdder = -1;
                extendTriIsAtFront = true;
                break;
            case RoofTriangleType.DownLeft:
                extendYAdder = -1;
                extendTriIsAtFront = false;
                break;
            case RoofTriangleType.DownRight:
                extendYAdder = 1;
                extendTriIsAtFront = true;
                break;
        }
    }

    public RoofTriangle getCopy()
    {
        return new RoofTriangle(points[0], points[1], points[2], TriType);
    }

    public RoofTriangleType TriType
    {
        get { return triType; }
    }
}

public enum RoofTriangleType
{
    UpLeft, UpRight, DownLeft, DownRight, Undetermined
}
