﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

// A Obstacle Point is the point, that can be painted by the player. The included data can be used to geerate the meshes.
public class ObstaclePoint
{
    [HideInInspector] public Vector2Int rasterPos;
    [HideInInspector] public GameObject placeHolder;
    [HideInInspector] public ObstaclePointType type = ObstaclePointType.Undetermined;
    [HideInInspector] public ObstaclePoint[] Neighbors; // has all Neighbors
    [HideInInspector] public ObstaclePoint BorderLeader = null;
    [HideInInspector] public ObstaclePoint BorderFollower = null;

    public ObstaclePoint(Vector2Int Position, GameObject PlaceHolder)
    {
        rasterPos = Position;
        spawnPlaceHolder(PlaceHolder);
    }

    void spawnPlaceHolder(GameObject Prefab)
    {
        placeHolder = UnityEngine.Object.Instantiate(Prefab, new Vector3(rasterPos.x, 0.02f, rasterPos.y), Quaternion.Euler(90, 0, 0));
    }

    public void deletePlaceHolder()
    {
        if (placeHolder != null)
        {
            UnityEngine.Object.Destroy(placeHolder);
            placeHolder = null;
        }
    }
}

public class ObstaclePointRaster
{
    ObstaclePoint[,] points = null;
    Vector2Int originPos = Vector2Int.zero;
    Vector2Int maxPos = Vector2Int.zero;

    // Constructors
    public ObstaclePointRaster(Vector2Int OriginPosition, Vector2Int RasterSize)
    {
        originPos = OriginPosition;
        maxPos = originPos + RasterSize - new Vector2Int(1, 1);
        points = new ObstaclePoint[RasterSize.x, RasterSize.y];
    }

    public ObstaclePointRaster(Vector2Int OriginPosition, ObstaclePoint[,] Points)
    {
        originPos = OriginPosition;
        maxPos = originPos + new Vector2Int(Points.GetLength(0) - 1, Points.GetLength(1) - 1);
        points = Points;
    }

    public ObstaclePointRaster(List<ObstaclePoint> pointList)
    {
        if (pointList.Count == 0) return;

        originPos.x = pointList[0].rasterPos.x;
        originPos.y = pointList[0].rasterPos.y;
        maxPos.x = pointList[0].rasterPos.x;
        maxPos.y = pointList[0].rasterPos.y;

        foreach (ObstaclePoint point in pointList)
        {
            if (point.rasterPos.x < originPos.x) originPos.x = point.rasterPos.x;
            if (point.rasterPos.y < originPos.y) originPos.y = point.rasterPos.y;
            if (point.rasterPos.x > maxPos.x) maxPos.x = point.rasterPos.x;
            if (point.rasterPos.y > maxPos.y) maxPos.y = point.rasterPos.y;
        }

        points = new ObstaclePoint[1 + maxPos.x - originPos.x, 1 + maxPos.y - originPos.y];

        foreach (ObstaclePoint point in pointList)
        {
            addPoint(point);
        }
    }


    // Point Funtions
    public ObstaclePoint findPoint(Vector2Int worldPos)
    {
        return findPoint(worldPos.x, worldPos.y);
    }
    public ObstaclePoint findPoint(int worldPosX, int worldPosY)
    {
        if (worldPosX > maxPos.x || worldPosX < originPos.x || worldPosY > maxPos.y || worldPosY < originPos.y)
        {
            return null;
        }
        else
        {
            return points[worldPosX - originPos.x, worldPosY - originPos.y];
        }
    }

    public void addPoint(ObstaclePoint point)
    {
        points[point.rasterPos.x - originPos.x, point.rasterPos.y - originPos.y] = point;
    }

    public void deletePoint(Vector2Int worldPos)
    {
        deletePoint(worldPos.x, worldPos.y);
    }
    public void deletePoint(int worldPosX, int worldPosY)
    {
        points[worldPosX - originPos.x, worldPosY - originPos.y].deletePlaceHolder();
        points[worldPosX - originPos.x, worldPosY - originPos.y] = null;
    }
    public void removePoint(Vector2Int worldPos)
    {
        points[worldPos.x - originPos.x, worldPos.y - originPos.y] = null;
    }

    public void deleteAllPoints()
    {
        for (int x = 0; x < points.GetLength(0); x++)
        {
            for (int y = 0; y < points.GetLength(1); y++)
            {
                if (points[x, y] != null) points[x, y].deletePlaceHolder();
            }
        }

        Array.Clear(points, 0, points.Length);
    }

    public void resetAllPoints()
    {
        foreach (ObstaclePoint point in points)
        {
            if (point != null)
            {
                point.type = ObstaclePointType.Undetermined;
                point.Neighbors = null;
                point.BorderLeader = null;
                point.BorderFollower = null;
            }
        }
    }


    // Getter
    public List<ObstaclePoint> getPointList()
    {
        List<ObstaclePoint> backList = new List<ObstaclePoint>();

        if (points == null) return backList;

        foreach (ObstaclePoint point in points)
        {
            if (point != null) backList.Add(point);
        }

        return backList;
    }

    public ObstaclePoint[,] getPointArray()
    {
        return points;
    }

    public ObstaclePointRaster getCopy()
    {
        return this.MemberwiseClone() as ObstaclePointRaster;
    }

    public bool hasPoints
    {
        get { return points != null; }
    }

    public Vector2Int arraySize
    {
        get { return new Vector2Int(points.GetLength(0), points.GetLength(1)); }
    }

    public Vector2Int OriginPosition
    {
        get { return originPos; }
    }

    public Vector2Int MaxPosition
    {
        get { return maxPos; }
    }
}

public enum ObstaclePointType
{
    Border, BorderChained, BorderSkip, Inside, Undetermined, Invalid
}
