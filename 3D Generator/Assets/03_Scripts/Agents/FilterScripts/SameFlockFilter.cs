﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Filter/SameFlock")]
public class SameFlockFilter : ContextFilter
{
    public override List<Transform> Filter(FlockAgent agent, List<Transform> originalContext)
    {
        List<Transform> filteredCntext = new List<Transform>();

        foreach (Transform item in originalContext)
        {
            FlockAgent itemAgent = item.GetComponent<FlockAgent>();
            if (itemAgent != null && itemAgent.agentFlock == agent.agentFlock)
            {
                filteredCntext.Add(item);
            }
        }

        return filteredCntext;
    }
}
