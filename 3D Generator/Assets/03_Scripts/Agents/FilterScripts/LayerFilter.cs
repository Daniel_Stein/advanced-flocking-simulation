﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Filter/LayerFilter")]
public class LayerFilter : ContextFilter
{
    [SerializeField] int[] targetLayer;

    public override List<Transform> Filter(FlockAgent agent, List<Transform> originalContext)
    {
        List<Transform> filteredContext = new List<Transform>();

        foreach (Transform item in originalContext)
        {
            foreach (int layer in targetLayer)
            {
                if (item.gameObject.layer == layer)
                {
                    filteredContext.Add(item);
                    break;
                }
            }
        }
        return filteredContext;
    }
}