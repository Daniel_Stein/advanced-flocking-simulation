﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaviour/Debug")]
public class DebugBehaviour : FilteredFlockBehaviour
{
    public override Vector3 calculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        if (agent.gameObject.name == "agent 0")
        {
            List<Transform> filteredContext = (filter == null) ? context : filter.Filter(agent, context);
            foreach (Transform item in filteredContext)
            {
                if (Vector3.SqrMagnitude(item.position - agent.transform.position) < flock.SquareAvoidanceRadius)
                {
                    Debug.DrawLine(agent.transform.position, item.position, Color.black);
                }
                if (Vector3.SqrMagnitude(item.position - agent.transform.position) > flock.SquareAvoidanceRadius)
                {
                    Debug.DrawLine(agent.transform.position, item.position, Color.white);
                }
            }
        }

        return Vector3.zero;
    }
}
