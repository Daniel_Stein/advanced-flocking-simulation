﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaviour/Composite")]
public class CompositeBehaviour : FlockBehaviour
{
    [SerializeField] public FlockBehaviour[] behaviours = new FlockBehaviour[0];
    [SerializeField] public float[] weights = new float[0];

    public override Vector3 calculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        if (behaviours.Length != weights.Length)
        {
            Debug.LogError("Data mismatch in " + name, this);
            return Vector3.zero;
        }

        //setup Move
        Vector3 move = Vector3.zero;

        //iterate through behaviours
        for (int i = 0; i < behaviours.Length; i++)
        {
            Vector3 partialMove = behaviours[i].calculateMove(agent, context, flock) * weights[i];

            if (partialMove != Vector3.zero)
            {
                move += partialMove;
            }

            //if (Selection.Contains(agent.gameObject))
            //{
            //    Debug.DrawRay(agent.transform.position + new Vector3(0,1f,0), partialMove, colors[i]);
            //}
        }

        //if (Selection.Contains(agent.gameObject))
        //{
        //    Debug.DrawRay(agent.transform.position + new Vector3(0, 1, 0), move, Color.black);
        //}

        move.y = 0;
        return move;
    }

    Color[] colors = new Color[] {Color.red, Color.blue, Color.yellow, Color.green, Color.magenta, Color.white};

}
