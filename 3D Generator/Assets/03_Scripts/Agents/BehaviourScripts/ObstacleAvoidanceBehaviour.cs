﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaviour/(Generated) Obstacle Avoidance")]
public class ObstacleAvoidanceBehaviour : FilteredFlockBehaviour
{
    public override Vector3 calculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        // if no neighbours, return no adjustment
        List<Transform> filteredContext = (filter == null) ? context : filter.Filter(agent, context);
        if (filteredContext.Count == 0)
        {
            return Vector3.zero;
        }

        // get nearest Point
        Vector2 avPoint2D = ClosestPointCalculator.getClosestPoint(agent.transform.position);

        // get avoidance Direction
        Vector2 forward2D = My_Math.ToVector2D(agent.transform.forward);
        Vector2 agentPos2D = My_Math.ToVector2D(agent.transform.position);
        float angle = Vector2.SignedAngle(forward2D, (agentPos2D - avPoint2D).normalized) + 180;

        //if (agent.gameObject.name == "agent 0") Debug.Log(angle);

        //Debug.DrawRay(agent.transform.position, agent.transform.forward * 5, Color.green);
        //Debug.DrawLine(agent.transform.position, My_Math.ToVector3D(avPoint2D), Color.red);

        Vector3 avoidanceMove = Vector3.zero;
        if (angle >= 0 && angle <= 90)
        {
            agent.turnSpeed = agent.baseTurnSpeed * 1.1f;
            avoidanceMove = agent.transform.right;
        }
        else if (angle >= 270 && angle <= 360)
        {
            agent.turnSpeed = agent.baseTurnSpeed * 1.1f;
            avoidanceMove = -agent.transform.right;
        }
        else
        {
            agent.turnSpeed = agent.baseTurnSpeed;
            avoidanceMove = agent.transform.forward;
        }

        avoidanceMove.y = 0;
        //Debug.DrawRay(agent.transform.position, avoidanceMove * 4, Color.blue);

        return avoidanceMove.normalized;
    }
}
