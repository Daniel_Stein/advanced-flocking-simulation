﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaviour/Cohesion")]
public class CohesionBehaviour : FilteredFlockBehaviour
{
    public override Vector3 calculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        // if no neighbours, return no adjustment
        if (context.Count == 0)
        {
            return Vector3.zero;
        }

        //add all points together and average
        Vector3 cohesionMove = Vector3.zero;
        int nCohese = 0;

        List<Transform> filteredContext = (filter == null) ? context : filter.Filter(agent, context);
        foreach (Transform item in filteredContext)
        {
            if (Vector3.SqrMagnitude(item.position - agent.transform.position) > flock.SquareAvoidanceRadius)
            {
                cohesionMove += item.position;
                nCohese++;
            }
        }
        cohesionMove /= nCohese;

        //create offset from agentPos
        cohesionMove -= agent.transform.position;
        cohesionMove.y = 0;

        return cohesionMove.normalized;
    }
}
