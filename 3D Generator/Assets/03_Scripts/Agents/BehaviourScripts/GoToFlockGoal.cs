﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaviour/GoToFlockGoal")]
public class GoToFlockGoal : FlockBehaviour
{
    public override Vector3 calculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        return (Flock.goalPosition - agent.transform.position).normalized;
    }
}
