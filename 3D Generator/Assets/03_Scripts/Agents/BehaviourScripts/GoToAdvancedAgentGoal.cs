﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaviour/GoToAdvancedAgentGoal")]
public class GoToAdvancedAgentGoal : FlockBehaviour
{
    public override Vector3 calculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        return ((agent as Agent_AdvancedFlock).leaderTransform.position - agent.transform.position).normalized;
    }
}
