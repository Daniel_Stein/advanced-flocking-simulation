﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaviour/Alignment")]
public class AlignmentBehaviour : FilteredFlockBehaviour
{
    public override Vector3 calculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        // if no neighbours, maintain cureent alignment
        if (context.Count == 0)
        {
            return agent.transform.forward;
        }

        //add all points together and average
        Vector3 alignmentDir = Vector3.zero;

        List<Transform> filteredContext = (filter == null) ? context : filter.Filter(agent, context);
        foreach (Transform item in filteredContext)
        {
            alignmentDir += item.forward;
        }
        alignmentDir /= filteredContext.Count;

        alignmentDir.y = 0;

        return alignmentDir.normalized;
    }
}
