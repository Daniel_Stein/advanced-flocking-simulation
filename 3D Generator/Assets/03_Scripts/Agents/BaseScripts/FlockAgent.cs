﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Collider)/*, typeof(Rigidbody)*/)]
public class FlockAgent : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField, Range(0f, 20f)] float baseMoveSpeed = 7;
    [SerializeField, Range(0f, 10f)] public float baseTurnSpeed = 7;
    [HideInInspector] public float turnSpeed;

    //Gameplay
    [HideInInspector] public bool intoPool = false;
    //[HideInInspector] public float deltaMovement = 10;
    Vector3 oldPos = Vector3.zero;

    [Header("References")]
    public Collider agentCollider;
    [HideInInspector] public Flock agentFlock;

    public virtual void Initilize(Flock flock, int layer, bool activateNavMeshAgent)
    {
        agentFlock = flock;
        intoPool = false;

        oldPos = transform.position;
        turnSpeed = baseTurnSpeed;

        GetComponent<NavMeshAgent>().enabled = activateNavMeshAgent;
        GetComponent<NavMeshAgent>().speed = baseMoveSpeed;
        gameObject.layer = layer;
    }

    public void move(Vector3 direction) // offsetTargetPosition
    {
        //Debug.DrawRay(transform.position, direction * 2, Color.black);

        Vector3 oldForward = transform.forward;
        Vector3 turnVel = Vector3.zero;

        transform.forward = Vector3.SmoothDamp(oldForward, direction, ref turnVel, 0.01f, turnSpeed).normalized;

        transform.position += transform.forward * baseMoveSpeed * Time.deltaTime;

        countMovedUnits();
    }

    public void countMovedUnits()
    {
        float frameMoveDistance = Vector3.Distance(oldPos, transform.position);
        //deltaMovement += frameMoveDistance;
        oldPos = transform.position;
    }


    // Getter Setter
    public float BaseMoveSpeed
    {
        get { return baseMoveSpeed; }
    }
}