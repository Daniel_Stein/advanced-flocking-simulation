﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Flock : MonoBehaviour
{
    [Header("Agent")]
    [SerializeField] protected FlockAgent agentPrefab;
    [SerializeField] public int layer;
    [SerializeField] protected bool activateNavMeshAgent;

    [Header("Flock")]
    [SerializeField] protected FlockBehaviour flockingBehaviour;
    [SerializeField] [Range(1f, 10f)] float neighbourRadius = 4f;
    [SerializeField] [Range(0f, 1f)] float avoidanceRadiusMultiplier = 0.75f;

    // Header("Level References
    public static Vector3 goalPosition = Vector3.zero;
    public static Vector2Int[] spawnPositions;

    [Header("Spawning & Updating")]
    [SerializeField] protected int spawnsPerFrame = 1;
    public static int spawnCircleRadius = 2;
    protected int spawnCount = 0;
    protected int agentIdentificationNumber = 0;

    protected List<FlockAgent> flockAgents = new List<FlockAgent>();
    protected List<FlockAgent> activeAgents = new List<FlockAgent>();
    protected List<FlockAgent> poolAgents = new List<FlockAgent>();

    public float SquareAvoidanceRadius { get { return Mathf.Pow((neighbourRadius * avoidanceRadiusMultiplier),2); } }


    protected virtual void Start()
    {
        StartCoroutine(checkForRespawn());
    }

    protected virtual void Update()
    {
        if (spawnCount > 0)
        {
            int spawnNumber = (spawnCount < spawnsPerFrame) ? spawnCount : spawnsPerFrame;
            for (int a = 0; a < spawnNumber; a++)
            {
                if(spawnAgent()) spawnCount--;
            }
        }

        if (flockAgents.Count > 0)
        {
            resortAgentsToList();
            doMovementOfAllAgents();
        }
    }


    // 0 - SpawnFunctions
    /// <summary>
    /// Public function for other classes to call, when agents shall be spawned.
    /// </summary>
    /// <param name="SpawnCount">How many agents shall be spawned.</param>
    /// <param name="SpawnPosition">Where shall the agents be spawned.</param>
    public void spawnAgents(int SpawnCount)
    {
        if (SpawnCount < 0)
        {
            Debug.Log("Sorry, but you can't spawn " + SpawnCount + " enemys!");
            return;
        }
        spawnCount += SpawnCount;
    }

    protected bool spawnAgent()
    {
        if (!findSpawnPosition(out Vector3 nextSpawnPos)) return false;

        Quaternion nextSpawnRot = Quaternion.Euler(Vector3.up * Random.Range(0f, 360f));

        FlockAgent tempAgent;
        if (poolAgents.Count > 0)
        {
            tempAgent = poolAgents[0];
            tempAgent.transform.position = nextSpawnPos;
            tempAgent.transform.rotation = nextSpawnRot;
            tempAgent.GetComponent<Rigidbody>().velocity = Vector3.zero;
            tempAgent.gameObject.SetActive(true);
            poolAgents.Remove(tempAgent);
        }
        else
        {
            tempAgent = Instantiate(agentPrefab, nextSpawnPos, nextSpawnRot, transform);
            tempAgent.name = "agent " + agentIdentificationNumber++;
            flockAgents.Add(tempAgent);
        }

        tempAgent.Initilize(this, layer, activateNavMeshAgent);

        return true;
    }

    protected bool findSpawnPosition(out Vector3 newSpawnPosition)
    {
        newSpawnPosition = Vector3.zero;

        bool back = false;
        for (int i = 0; i < spawnPositions.Length; i++)
        {
            newSpawnPosition = new Vector3(spawnPositions[i].x, 0.5f, spawnPositions[i].y);
            if (Physics.OverlapSphere(newSpawnPosition, 0.75f, 1 << layer).Length == 0)
            {
                back = true;
                break;
            }
        }
        return back;
    }


    // 1 - sort agents
    protected void resortAgentsToList()
    {
        activeAgents.Clear();
        poolAgents.Clear();

        foreach (FlockAgent agent in flockAgents)
        {
            if (agent.intoPool)
            {
                agent.gameObject.SetActive(false);
                agent.transform.position = new Vector3(0, -10, 0);
                poolAgents.Add(agent);
            }
            else
            {
                activeAgents.Add(agent);
            }
        }
    }


    // 2 - MoveFunctions
    protected virtual void doMovementOfAllAgents()
    {
        foreach (FlockAgent agent in activeAgents)
        {
            List<Transform> context = getNearbyObjects(agent);

            Vector3 move = flockingBehaviour.calculateMove(agent, context, this);
            agent.move(move.normalized);
        }
    }

    protected List<Transform> getNearbyObjects(FlockAgent agent)
    {
        List<Transform> context = new List<Transform>();

        Collider[] contextColliders = Physics.OverlapSphere(agent.transform.position, neighbourRadius);

        foreach (Collider col in contextColliders)
        {
            if (col != agent.agentCollider)
            {
                context.Add(col.transform);
            }
        }
        return context;
    }


    // 3 - RespawnFunctions (Not in Update, but in Coroutine)
    IEnumerator checkForRespawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);

            foreach (FlockAgent agent in activeAgents)
            {
                if (agent.transform.position.y < -20/* || agent.deltaMovement < agent.BaseMoveSpeed / 4*/)
                {
                    Debug.DrawLine(agent.transform.position, agent.transform.position + Vector3.up * 3, Color.red, 300);
                    respawnAgent(agent);
                }
                //agent.deltaMovement = 0;
            }
        }
    }

    void respawnAgent(FlockAgent agent)
    {
        agent.intoPool = true;
        spawnAgents(1);
    }


    // 4- Remove all agents from Map
    public void putAllAgentsIntoPool()
    {
        spawnCount = 0;

        foreach (FlockAgent agent in activeAgents)
        {
            agent.intoPool = true;
        }
    }
}