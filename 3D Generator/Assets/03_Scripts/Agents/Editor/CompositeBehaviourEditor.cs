﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

[CustomEditor(typeof(CompositeBehaviour))]
public class CompositeBehaviourEditor : Editor
{
    SerializedProperty seriBehaviours;
    SerializedProperty seriWeights;

    public void OnEnable()
    {
        seriBehaviours = serializedObject.FindProperty("behaviours");
        seriWeights = serializedObject.FindProperty("weights");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.Space();

        if (seriWeights.arraySize != seriBehaviours.arraySize)
        {
            EditorGUILayout.HelpBox("The number of behaviors und weights are different. How the f*** did that happened? Tell Daniel what you did.", MessageType.Error);

            if (GUILayout.Button("Reset Behaviour"))
            {
                resetArrays();
            }
            return;
        }

        if (seriWeights.arraySize < 1)
        {
            EditorGUILayout.HelpBox("There are no behaviours", MessageType.Info);
        }
        else
        {
            float windowWidth = EditorGUIUtility.currentViewWidth;

            float labelWidth = 65;
            float widthB = windowWidth * 0.6f - labelWidth - 18;
            float widthW = windowWidth * 0.4f - labelWidth - 18;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Behaviours", EditorStyles.boldLabel, GUILayout.Width(labelWidth + widthB));
            EditorGUILayout.LabelField("Weights", EditorStyles.boldLabel);
            EditorGUILayout.EndHorizontal();

            for (int i = 0; i < seriBehaviours.arraySize; i++)
            {
                GUILayout.BeginHorizontal();

                EditorGUILayout.LabelField("Behavior " + i, GUILayout.Width(labelWidth));
                EditorGUILayout.PropertyField(seriBehaviours.GetArrayElementAtIndex(i), GUIContent.none, GUILayout.Width(widthB));

                EditorGUILayout.LabelField("Weigth " + i, GUILayout.Width(labelWidth));
                EditorGUILayout.PropertyField(seriWeights.GetArrayElementAtIndex(i), GUIContent.none, GUILayout.Width(widthW));

                GUILayout.EndHorizontal();
            }
        }

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Add Behaviour"))
        {
            addBehaviour();
        }
        if (GUILayout.Button("Remove Behaviour"))
        {
            removeBehaviour();
        }
        GUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
    }

    void addBehaviour()
    {
        FlockBehaviour[] oldBeh = ((CompositeBehaviour)target).behaviours;
        float[] oldWei = ((CompositeBehaviour)target).weights;

        int oldLength = oldBeh.Length;
        int newLength = oldLength + 1;

        FlockBehaviour[] newBeh = new FlockBehaviour[newLength];
        float[] newWei = new float[newLength];

        for (int i = 0; i < oldLength; i++)
        {
            newBeh[i] = oldBeh[i];
            newWei[i] = oldWei[i];
        }
        newBeh[oldLength] = null;
        newWei[oldLength] = 0;

        ((CompositeBehaviour)target).behaviours = newBeh;
        ((CompositeBehaviour)target).weights = newWei;

        serializedObject.ApplyModifiedProperties();
    }

    void removeBehaviour()
    {
        FlockBehaviour[] oldBeh = ((CompositeBehaviour)target).behaviours;
        float[] oldWei = ((CompositeBehaviour)target).weights;

        int oldLength = oldBeh.Length;
        int newLength = oldLength - 1;

        if (oldLength == 0) return;

        FlockBehaviour[] newBeh = new FlockBehaviour[newLength];
        float[] newWei = new float[newLength];

        for (int i = 0; i < newLength; i++)
        {
            newBeh[i] = oldBeh[i];
            newWei[i] = oldWei[i];
        }

        ((CompositeBehaviour)target).behaviours = newBeh;
        ((CompositeBehaviour)target).weights = newWei;

        serializedObject.ApplyModifiedProperties();
    }

    void resetArrays()
    {
        seriBehaviours.ClearArray();
        seriWeights.ClearArray();
        serializedObject.ApplyModifiedProperties();
    }
}