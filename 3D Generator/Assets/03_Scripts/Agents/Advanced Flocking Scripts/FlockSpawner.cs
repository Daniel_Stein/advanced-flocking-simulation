﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;

public class FlockSpawner : MonoBehaviour
{
    [Header("Flocking Scene Refernces")]
    [SerializeField] Transform startPoint;
    [SerializeField] Transform goalPoint;

    [Header("Script References")]
    [SerializeField] FlockingScene_UiManager uiManager;
    [SerializeField] FlockingScene_ObstaclePainter obstaclePainter;
    [SerializeField] Flock flock_pureFlocking;
    [SerializeField] Flock_PurePathfinding flock_purePathfinding;
    [SerializeField] Flock_AdvancedFlock flock_advancedFlocking;

    // Values
    [HideInInspector] public bool goalPointIsPlaced = false;
    [HideInInspector] public bool startPointIsPlaced = false;
    int spawnCount;
    FlockType flockType = FlockType.None;

    private void Start()
    {
        uiManager.showIfStartPointIsPlaced(startPointIsPlaced = false);
        uiManager.showIfGoalPointIsPlaced(goalPointIsPlaced = false);
    }


    #region SpawnPosition and EndPosition

    public Vector3 SpawnPosition
    {
       // get { return startPoint.position; }
        set
        {
            startPoint.position = value;
            uiManager.showIfStartPointIsPlaced(startPointIsPlaced = true);
        }
    }

    public Vector3 GoalPosition
    {
        get { return Flock.goalPosition; }
        set
        {
            if (Physics.OverlapSphere(value, 1.5f, 1 << 8).Length == 0)
            {
                Flock.goalPosition = value;
                goalPoint.position = value;
                uiManager.showIfGoalPointIsPlaced(goalPointIsPlaced = true);

                flock_purePathfinding.refreshNavMeshAgentTarget();
                flock_advancedFlocking.refreshNavMeshAgentTarget();
            }
        }
    }

    public int setSpawnCircleRadius(int Radius)
    {
        if (obstaclePainter.hasPaintedPoint(startPoint.position, Radius, out Vector2Int[] affectedPoints))
        {
            return Flock.spawnCircleRadius;
        }

        Flock.spawnCircleRadius = Radius;
        Flock.spawnPositions = affectedPoints;
        startPoint.localScale = new Vector3(Radius * 2, Radius * 2, 1);

        return Radius;
    }

    public int setSpawnCircleRadiusAtStart(int Radius)
    {
        Flock.spawnCircleRadius = Radius;
        startPoint.localScale = new Vector3(Radius * 2, Radius * 2, 1);
        return Radius;
    }

    #endregion


    #region Spawning

    public int SpawnCount
    {
        get { return spawnCount; }
        set
        {
            spawnCount = value > 0 ? value : 0;
        }
    }

    public void setFlockType(int typeNumber)
    {
        flockType = (FlockType)typeNumber;
    }

    public void spawnFlock()
    {
        if (!goalPointIsPlaced || !startPointIsPlaced) return;

        switch (flockType)
        {
            case FlockType.PureFlocking:
                flock_pureFlocking.spawnAgents(SpawnCount);
                break;
            case FlockType.PurePathfinding:
                flock_purePathfinding.spawnAgents(SpawnCount);
                break;
            case FlockType.AdvacedFlocking:
                flock_advancedFlocking.spawnAgents(SpawnCount);
                break;
            default:
                break;
        }
    }

    public void deleteFlock()
    {
        flock_pureFlocking.putAllAgentsIntoPool();
        flock_purePathfinding.putAllAgentsIntoPool();
        flock_advancedFlocking.putAllAgentsIntoPool();
    }

    #endregion


    public enum FlockType
    {
        PureFlocking, PurePathfinding, AdvacedFlocking, None
    }
}
