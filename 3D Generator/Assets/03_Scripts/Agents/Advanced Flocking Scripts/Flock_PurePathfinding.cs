﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Flock_PurePathfinding : Flock
{
    protected override void Update()
    {
        bool spawnedNewAgents = false;
        if (spawnCount > 0)
        {
            int spawnNumber = (spawnCount < spawnsPerFrame) ? spawnCount : spawnsPerFrame;
            for (int a = 0; a < spawnNumber; a++)
            {
                if (spawnAgent())
                {
                    spawnCount--;
                    spawnedNewAgents = true;
                }
            }
        }

        if (flockAgents.Count > 0)
        {
            resortAgentsToList();
            doMovementOfAllAgents();
        }

        if (spawnedNewAgents) refreshNavMeshAgentTarget();
    }

    public void refreshNavMeshAgentTarget()
    {
        foreach (FlockAgent agent in activeAgents)
        {
            NavMeshAgent nma = agent.GetComponent<NavMeshAgent>();
            if (nma.enabled) nma.SetDestination(goalPosition);
        }
    }

    protected override void doMovementOfAllAgents()
    {
        foreach (FlockAgent agent in activeAgents)
        {
            agent.countMovedUnits();
        }
    }
}
