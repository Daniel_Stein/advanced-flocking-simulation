﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Agent_AdvancedFlock : FlockAgent
{
    // Advanced Flcoking
    [HideInInspector] public Transform leaderTransform = null;
    [HideInInspector] public bool isLeader = false;
    [HideInInspector] public float sqrDisToLeader = -1;

    public override void Initilize(Flock flock, int layer, bool activateNavMeshAgent)
    {
        base.Initilize(flock, layer, activateNavMeshAgent);
        InitilizeAdvanced(false, null);
    }

    public void InitilizeAdvanced(bool IsLeader, Transform LeaderTransform, float SqrDisToLeader = -1)
    {
        isLeader = IsLeader;
        sqrDisToLeader = SqrDisToLeader;
        GetComponent<NavMeshAgent>().enabled = isLeader;
        leaderTransform = LeaderTransform;

        if (IsLeader)
            gameObject.layer = 13;
        else
            gameObject.layer = agentFlock.layer;
    }
}
