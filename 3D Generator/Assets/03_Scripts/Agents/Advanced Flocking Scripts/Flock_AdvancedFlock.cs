﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class Flock_AdvancedFlock : Flock
{
    List<Agent_AdvancedFlock> followerAgents = new List<Agent_AdvancedFlock>();
    List<Agent_AdvancedFlock> leaderAgents = new List<Agent_AdvancedFlock>();
    Agent_AdvancedFlock mainLeaderAgent = null;

    protected override void Start()
    {

    }

    protected override void Update()
    {
        bool spawnedNewAgents = false;

        if (spawnCount > 0)
        {
            int spawnNumber = (spawnCount < spawnsPerFrame) ? spawnCount : spawnsPerFrame;
            for (int a = 0; a < spawnNumber; a++)
            {
                if (spawnAgent())
                {
                    spawnCount--;
                    spawnedNewAgents = true;
                }
            }
        }

        if (flockAgents.Count > 0)
        {
            resortAgentsToList();
            resortActiveAgentsToLists();
            setLeaderNet2();

            doMovementOfAllAgents();
        }

        if (spawnedNewAgents) refreshNavMeshAgentTarget();

        foreach (var agent in activeAgents)
        {
            Agent_AdvancedFlock aAgent = (Agent_AdvancedFlock)agent;
            if (aAgent.leaderTransform != null)
            {
                Debug.DrawLine(aAgent.transform.position, aAgent.leaderTransform.position, Color.blue);
            }
            else
            {
                Debug.DrawRay(aAgent.transform.position, Vector3.up * 4, Color.green);
            }
        }
    }

    // 1.2 - sort agents
    void resortActiveAgentsToLists()
    {
        followerAgents.Clear();
        leaderAgents.Clear();

        foreach (FlockAgent agent in activeAgents)
        {
            Agent_AdvancedFlock temp = (Agent_AdvancedFlock)agent;
            if (temp.isLeader)
            {
                leaderAgents.Add(temp);
            }
            else
            {
                followerAgents.Add(temp);
            }
        }

        if (leaderAgents.Count == 0 && activeAgents.Count > 0)
        {
            mainLeaderAgent = (Agent_AdvancedFlock)activeAgents[0];
            mainLeaderAgent.InitilizeAdvanced(true, null);
            leaderAgents.Add(mainLeaderAgent);
            followerAgents.Remove(mainLeaderAgent);
        }
    }

    void setLeaderNet2()
    {
        foreach (Agent_AdvancedFlock agent in leaderAgents)
        {
            if (agent == mainLeaderAgent) continue;

            if (isTargetVisible(agent.transform, mainLeaderAgent.transform))
            {
                agent.InitilizeAdvanced(false, mainLeaderAgent.transform);
            }
        }

        List<Agent_AdvancedFlock> followerWithLeader = new List<Agent_AdvancedFlock>();
        List<Agent_AdvancedFlock> followerWithoutLeader = new List<Agent_AdvancedFlock>();
        foreach (var agent in followerAgents)
        {
            if (isTargetVisible(agent.transform, mainLeaderAgent.transform))
            {
                float dis = Vector3.SqrMagnitude(agent.transform.position - mainLeaderAgent.transform.position);
                agent.InitilizeAdvanced(false, mainLeaderAgent.transform, dis);
                followerWithLeader.Add(agent);
            }
            else
            {
                agent.InitilizeAdvanced(false, null);
                followerWithoutLeader.Add(agent);
            }
        }

        while (followerWithLeader.Count != 0 && followerWithoutLeader.Count != 0)
        {
            List<Agent_AdvancedFlock> justGotLeader = new List<Agent_AdvancedFlock>();

            foreach (var agent in followerWithoutLeader)
            {
                bool agentHasLeader = false;

                foreach (var tempLeader in followerWithLeader)
                {
                    if (tempLeader == agent) continue; // nur zur sicherheit, dürfte aber nicht passieren

                    if (isTargetVisible(agent.transform, tempLeader.transform))
                    {
                        float newDisToLeader = tempLeader.sqrDisToLeader + Vector3.SqrMagnitude(agent.transform.position - tempLeader.transform.position);
                        if (agent.sqrDisToLeader == -1 || agent.sqrDisToLeader > newDisToLeader)
                        {
                            agent.InitilizeAdvanced(false, tempLeader.transform, newDisToLeader);
                            agentHasLeader = true;
                        }
                    }
                }

                if (agentHasLeader) justGotLeader.Add(agent);
            }

            followerWithLeader.Clear();
            followerWithLeader.AddRange(justGotLeader);
            followerWithoutLeader = followerWithoutLeader.Except(justGotLeader).ToList();
        }

        foreach (var agent in followerWithoutLeader)
        {
            if (agent.leaderTransform == null)
            {
                foreach (var tempLeader in leaderAgents)
                {
                    float newSqrDis = Vector3.SqrMagnitude(tempLeader.transform.position - agent.transform.position);
                    if ((agent.sqrDisToLeader == -1 || agent.sqrDisToLeader > newSqrDis) && isTargetVisible(agent.transform, tempLeader.transform))
                    {
                        agent.InitilizeAdvanced(false, tempLeader.transform, newSqrDis);
                    }
                }
            }

            if (agent.leaderTransform == null)
            {
                agent.InitilizeAdvanced(true, null);
                leaderAgents.Add(agent);
                followerAgents.Remove(agent);
                refreshNavMeshAgentTarget();
            }
        }
    }


    bool isTargetVisible(Transform Agent, Transform Target)
    {
        Vector3 agentPos = Agent.position;
        Vector3 targetPos = Target.position;
        Vector3 rayDirection = targetPos - agentPos;
        float rayDistance = rayDirection.magnitude;

        Ray ray = new Ray(agentPos, rayDirection);
        Physics.Raycast(ray, out RaycastHit hit, rayDistance, 1 << 8);

        return hit.collider == null;
    }


    // 2.2 - MoveFunctions
    protected override void doMovementOfAllAgents()
    {
        foreach (FlockAgent agent in followerAgents)
        {
            List<Transform> context = getNearbyObjects(agent);

            Vector3 move = flockingBehaviour.calculateMove(agent, context, this);
            agent.move(move.normalized);
        }

        foreach (Agent_AdvancedFlock agent in leaderAgents)
        {
            agent.countMovedUnits();
        }
    }

    public void refreshNavMeshAgentTarget()
    {
        foreach (FlockAgent agent in leaderAgents)
        {
            NavMeshAgent nma = agent.GetComponent<NavMeshAgent>();
            if (nma.enabled) nma.SetDestination(goalPosition);
        }
    }
}