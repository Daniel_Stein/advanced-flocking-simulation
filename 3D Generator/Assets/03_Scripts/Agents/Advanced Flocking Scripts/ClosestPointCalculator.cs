﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.ConstrainedExecution;
using UnityEngine;

public static class ClosestPointCalculator
{
    static List<ObstaclePointGroup> allPointGroups;
    static Vector2 areaSize;

    public static Vector2 getClosestPoint(Vector3 AgentPosition)
    {
        Vector2 agentPos2D = My_Math.ToVector2D(AgentPosition);

        Vector2 CP1 = getClosestPointFromBorderList(agentPos2D, out float dis1);
        Vector2 CP2 = getClosestPointFromArenaBorder(agentPos2D, out float dis2);

        Vector2 CP = (dis1 == -1 || dis2 < dis1) ? CP2 : CP1;
        return CP;
    }

    static Vector2 getClosestPointFromBorderList(Vector2 Pos, out float smallestDistance)
    {
        Vector2 back;

        if (allPointGroups == null || allPointGroups.Count == 0)
        {
            back = Vector2.zero;
            smallestDistance = -1;
        }
        else
        {
            back = allPointGroups[0].BorderMeshLines[0][0];
            smallestDistance = Vector2.SqrMagnitude(Pos - back);

            foreach (ObstaclePointGroup pointGroup in allPointGroups)
            {
                foreach (List<Vector2Int> meshLine in pointGroup.BorderMeshLines)
                {
                    for (int i = 0; i < meshLine.Count - 1; i++)
                    {
                        float tempDistance = getSqrDistanceFromLine(Pos, meshLine[i], meshLine[i + 1], out Vector2 tempPoint);
                        if (tempDistance < smallestDistance)
                        {
                            smallestDistance = tempDistance;
                            back = tempPoint;
                        }
                    }
                }
            }
        }
        return back;
    }

    static Vector2 getClosestPointFromArenaBorder(Vector2 Pos, out float smallestDistance)
    {
        Vector2 back = Vector2.zero;
        smallestDistance = Vector2.SqrMagnitude(Pos - back);

        Vector2[] borderPoints = new Vector2[] { Vector2.zero, new Vector2(areaSize.x, 0), areaSize, new Vector2(0, areaSize.y), Vector2.zero };

        for (int i = 0; i < 4; i++)
        {
            float tempDis = getSqrDistanceFromLine(Pos, borderPoints[i], borderPoints[i + 1], out Vector2 closestPoint);
            if (tempDis < smallestDistance)
            {
                back = closestPoint;
                smallestDistance = tempDis;
            }
        }

        return back;
    }

    static float getSqrDistanceFromLine(Vector2 Pos, Vector2 P1, Vector2 P2, out Vector2 closestPoint)
    {
        closestPoint = Vector2.zero;
        float l2 = Vector2.SqrMagnitude(P1 - P2);
        if (l2 == 0.0) return Vector2.SqrMagnitude(Pos - P1);
        float t = Mathf.Max(0, Mathf.Min(1, Vector2.Dot(Pos - P1, P2 - P1) / l2));
        Vector2 projection = P1 + t * (P2 - P1);
        closestPoint = projection;
        return Vector2.SqrMagnitude(Pos - projection);
    }


    // set Value Functions
    public static void setBorderList(List<ObstaclePointGroup> pointGroups)
    {
        allPointGroups = pointGroups;
    }

    public static void setAreaSize(Vector2 Size)
    {
        areaSize = Size;
    }
}

