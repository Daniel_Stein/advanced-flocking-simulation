﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnusedCode
{
    public class NotUsedCode : MonoBehaviour
    {
        #region First Version for Obsatcle Roof Generation
        /*
        float obsatcleHight = 1;

        List<ObstaclePointGroup> obstaclePointGroups;
        struct ObstaclePointGroup
        {
            public List<ObstaclePoint> AllPoints;
            public List<ObstaclePoint> BorderPoints;
            public List<List<Vector2Int>> BorderMeshLines;
            public List<Vector2Int> FreeNeighborPositions;
        }

        void generateRoof()
        {
            foreach (ObstaclePointGroup pointGroup in obstaclePointGroups)
            {
                // For Simple Obsatcles
                if (pointGroup.BorderMeshLines.Count == 1 && hasFittingMiddlePoint(pointGroup, out Vector2 MiddlePoint))
                {
                    List<Vector2Int> meshLine = pointGroup.BorderMeshLines[0];

                    for (int i = 0; i < meshLine.Count - 1; i++)
                    {
                        Vector3 P1 = new Vector3(meshLine[i].x, obsatcleHight, meshLine[i].y);
                        Vector3 P2 = new Vector3(meshLine[i + 1].x, obsatcleHight, meshLine[i + 1].y);
                        Vector3 MP = new Vector3(MiddlePoint.x, obsatcleHight, MiddlePoint.y);

                        //addTriangle(P1, MP, P2);
                    }
                }
                else
                {
                    // New Version of Roof generation
                }
            }
        }

        bool hasFittingMiddlePoint(ObstaclePointGroup pointGroup, out Vector2 fittingPoint)
        {
            List<Vector2Int> borderLine = pointGroup.BorderMeshLines[0];
            List<Vector2> posMidPoints = new List<Vector2>();

            Vector2 mp = Vector2.zero;
            for (int i = 0; i < borderLine.Count - 1; i++) mp += borderLine[i];
            mp /= borderLine.Count - 1;
            posMidPoints.Add(mp);

            for (int i = 0; i < pointGroup.AllPoints.Count; i++)
            {
                if (pointGroup.AllPoints[i].type == ObstaclePointType.Inside)
                {
                    posMidPoints.Add(pointGroup.AllPoints[i].rasterPos);
                }
            }

            for (int mid = 0; mid < posMidPoints.Count; mid++)
            {
                for (int act = 0; act < borderLine.Count - 1; act++)
                {
                    for (int check = 0; check < borderLine.Count - 1; check++)
                    {
                        if (borderLine[act] != borderLine[check] && borderLine[act] != borderLine[check + 1])
                        {
                            if (areLinesIntersecting(borderLine[act], posMidPoints[mid], borderLine[check], borderLine[check + 1]))
                            {
                                goto EndCheck;
                            }
                        }
                    }
                }
                fittingPoint = posMidPoints[mid];
                return true;

                EndCheck:;
            }

            fittingPoint = Vector2.zero;
            return false;
        }

        bool areLinesIntersecting(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
        {
            // Get the segments' parameters.
            float dx12 = p2.x - p1.x;
            float dy12 = p2.y - p1.y;
            float dx34 = p4.x - p3.x;
            float dy34 = p4.y - p3.y;

            float denominator = (dy12 * dx34 - dx12 * dy34); // Solve for t1 and t2

            float t1 = ((p1.x - p3.x) * dy34 + (p3.y - p1.y) * dx34) / denominator;
            if (float.IsInfinity(t1)) return false; // The lines are parallel (or close enough to it).

            float t2 = ((p3.x - p1.x) * dy12 + (p1.y - p3.y) * dx12) / -denominator;

            return ((t1 >= 0) && (t1 <= 1) && (t2 >= 0) && (t2 <= 1)); // The segments intersect if t1 and t2 are between 0 and 1.
        }
        */
        #endregion

        #region Vertex Shower
        /*
        [SerializeField, Range(0f, 0.1f)] float sphereSize = 0.1f;
        [SerializeField, Range(0f, 1f)] float lineLength = 1f;

        private void OnDrawGizmos()
        {
            Mesh mesh = GetComponent<MeshFilter>().mesh;

            if (mesh != null)
            {
                Debug.Log("VertexCount = " + mesh.vertexCount);
                Gizmos.color = Color.green;
                for (int i = 0; i < mesh.vertexCount; i++)
                {
                    if (sphereSize != 0) Gizmos.DrawSphere(transform.position + mesh.vertices[i], sphereSize);
                    if (lineLength != 0) Debug.DrawRay(transform.position + mesh.vertices[i], mesh.normals[i] * lineLength);
                }
            }
        }
        */
        #endregion

        #region Old Follower Leader Net Setter
        /*
        void setLeaderNet()
        {
            foreach (Agent_AdvancedFlock agent in leaderAgents)
            {
                if (agent == mainLeaderAgent) continue;

                if (isTargetVisible(agent.transform, mainLeaderAgent.transform))
                {
                    agent.InitilizeAdvanced(false, mainLeaderAgent.transform);
                }
            }

            List<Agent_AdvancedFlock> followerWithLeader = new List<Agent_AdvancedFlock>();
            List<Agent_AdvancedFlock> agentsCouldGetBetterLeader = new List<Agent_AdvancedFlock>();
            foreach (var agent in followerAgents)
            {
                if (isTargetVisible(agent.transform, mainLeaderAgent.transform))
                {
                    if (agent.leaderTransform != mainLeaderAgent.transform)
                        agent.InitilizeAdvanced(false, mainLeaderAgent.transform);

                    followerWithLeader.Add(agent);
                }
                else if (agent.leaderTransform != null && isTargetVisible(agent.transform, agent.leaderTransform))
                {
                    followerWithLeader.Add(agent);
                    agentsCouldGetBetterLeader.Add(agent);
                }
                else
                {
                    agent.InitilizeAdvanced(false, null);
                    agentsCouldGetBetterLeader.Add(agent);
                }
            }

            foreach (var agent in agentsCouldGetBetterLeader)
            {
                float disToLeader = -1;
                foreach (var tempLeader in followerWithLeader)
                {
                    if (tempLeader == agent) continue;

                    if (isTargetVisible(agent.transform, tempLeader.transform))
                    {
                        float newDisToLeader = getChainDistaceToLeader(agent, tempLeader);
                        if (newDisToLeader != -1 && (disToLeader == -1 || disToLeader > newDisToLeader))
                        {
                            disToLeader = newDisToLeader;
                            agent.InitilizeAdvanced(false, tempLeader.transform);
                        }
                    }
                }

                if (agent.leaderTransform == null)
                {
                    float sqrDis = -1;
                    foreach (var tempLeader in leaderAgents)
                    {
                        float newSqrDis = Vector3.SqrMagnitude(tempLeader.transform.position - agent.transform.position);
                        if ((sqrDis == -1 || sqrDis > newSqrDis) && isTargetVisible(agent.transform, tempLeader.transform))
                        {
                            sqrDis = newSqrDis;
                            agent.InitilizeAdvanced(false, tempLeader.transform);
                        }
                    }
                }

                if (agent.leaderTransform == null)
                {
                    agent.InitilizeAdvanced(true, null);
                    leaderAgents.Add(agent);
                    followerAgents.Remove(agent);
                    refreshNavMeshAgentTarget();
                }
            }
        }

        float getChainDistaceToLeader(Agent_AdvancedFlock Agent, Agent_AdvancedFlock leader1)
        {
            float back = Vector3.SqrMagnitude(Agent.transform.position - leader1.transform.position);

            List<Agent_AdvancedFlock> checkedLeaders = new List<Agent_AdvancedFlock>() { Agent, leader1 };
            Agent_AdvancedFlock leader2 = leader1.leaderTransform.GetComponent<Agent_AdvancedFlock>();

            while (leader1 != mainLeaderAgent && leader2 != null)
            {
                back += Vector3.SqrMagnitude(leader1.transform.position - leader2.transform.position);

                if (checkedLeaders.Contains(leader2))
                {
                    back = -1;
                    break;
                }

                if (leader2.leaderTransform == null) break;

                checkedLeaders.Add(leader2);
                leader1 = leader2;
                leader2 = leader2.leaderTransform.GetComponent<Agent_AdvancedFlock>();
            }

            return back;
        }
        */
        #endregion

    }

}
