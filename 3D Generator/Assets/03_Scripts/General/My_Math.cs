﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class My_Math
{
    public static Vector2 ToVector2D(Vector3 Vector3D)
    {
        return new Vector2(Vector3D.x, Vector3D.z);
    }

    public static Vector3 ToVector3D(Vector2 Vector2D)
    {
        return new Vector3(Vector2D.x, 0, Vector2D.y);
    }
}
