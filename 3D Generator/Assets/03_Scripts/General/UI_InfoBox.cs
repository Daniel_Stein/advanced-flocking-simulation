﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_InfoBox : MonoBehaviour
{
    [SerializeField] Text titleText;
    [SerializeField] Text infoText;
    [SerializeField] CanvasGroup canvasGroup;

    public void showInfoBox(Vector2 Position, float infoTextHeight, string TitleText, string InfoText)
    {
        GetComponent<RectTransform>().position = Position;
        GetComponent<RectTransform>().sizeDelta = new Vector2(400, infoTextHeight + 40);
        titleText.text = TitleText;
        infoText.text = InfoText;
        infoText.GetComponent<RectTransform>().sizeDelta = new Vector2(400, infoTextHeight);

        LeanTween.alphaCanvas(canvasGroup, 1, 0.1f);
    }

    public void hideInfoBox()
    {
        LeanTween.alphaCanvas(canvasGroup, 0, 0.15f);
    }
}
