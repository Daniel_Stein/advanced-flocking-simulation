﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] Text cameraModeText;
    [SerializeField] MeshRenderer[] meshRenderers;
    [SerializeField] ObstacleGenerator obstacleGenerator;

    [Header("Script References")]
    [SerializeField] UiManagerBase uiManger;

    [Header("Movement Values")]
    [SerializeField] float moveSpeed = 10;
    [SerializeField] float turnSpeed = 5;
    [SerializeField] float maxLookUpAngle = 90;
    [SerializeField] float maxLookDownAngle = 90;

    bool typingStuff = false;
    Vector3 rotation = Vector3.zero;
    CameraModes camMode = CameraModes.FirstPerson;

    CharacterController charCon;
    Camera camera;

    // Start is called before the first frame update
    void Start()
    {
        charCon = GetComponent<CharacterController>();
        camera = GetComponentInChildren<Camera>();
        setCameraModeToFirstPerson();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Time.timeScale = Time.timeScale == 0 ? 1 : 0;
        }


        if (!typingStuff)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1)) setCameraModeToFirstPerson();
            if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2)) setCameraModeToFlyingPerson();
            if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3)) setCameraModeToTopDownView();

            Movement_General();

            switch (camMode)
            {
                case CameraModes.FirstPerson:
                    Movement_FirstPerson();
                    break;
                case CameraModes.FlyingPerson:
                    Movement_FlyingPerson();
                    break;
                case CameraModes.TopDownView:
                    Movement_TopDownView();
                    break;
                default:
                    break;
            }
        }
    }


    #region Movement
    void Movement_General()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            moveSpeed *= 2;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            moveSpeed /= 2;
        }

        //if (Input.GetKeyDown(KeyCode.Q) && camMode != CameraModes.TopDownView)
        //{
        //    switchCameraRotatable();
        //}

        if (!Cursor.visible)
        {
            rotation.x += Input.GetAxis("Mouse Y") * -turnSpeed;
            rotation.y += Input.GetAxis("Mouse X") * turnSpeed;
            if (rotation.x > maxLookDownAngle) rotation.x = maxLookDownAngle;
            if (rotation.x < -maxLookUpAngle) rotation.x = -maxLookUpAngle;
            camera.transform.rotation = Quaternion.Euler(rotation);
        }

        if (transform.position.y < -40)
        {
            resetPos();
            setCameraModeToFirstPerson();
        }
    }

    void Movement_FirstPerson()
    {
        Vector3 forward = camera.transform.forward;
        Vector3 right = camera.transform.right;

        forward.y = 0;
        forward.Normalize();

        charCon.SimpleMove(forward * moveSpeed * Input.GetAxis("Vertical"));
        charCon.SimpleMove(right * moveSpeed * Input.GetAxis("Horizontal"));
    }

    void Movement_FlyingPerson()
    {
        Vector3 forward = camera.transform.forward;
        Vector3 right = camera.transform.right;
        Vector3 up = new Vector3(0, 1, 0);

        transform.Translate(forward * moveSpeed * Input.GetAxis("Vertical") * Time.deltaTime);
        transform.Translate(right * moveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime);

        if (Input.GetKey(KeyCode.E)) transform.Translate(up * moveSpeed * Time.deltaTime);
        if (Input.GetKey(KeyCode.Q)) transform.Translate(-up * moveSpeed * Time.deltaTime);
    }

    void Movement_TopDownView()
    {
        Vector3 forward = new Vector3(0, 0, 1);
        Vector3 right = new Vector3(1, 0, 0);

        transform.Translate(forward * moveSpeed * Input.GetAxis("Vertical") * Time.deltaTime);
        transform.Translate(right * moveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime);

        camera.orthographicSize += Input.GetAxis("Mouse ScrollWheel") * -5;
        if (camera.orthographicSize < 1) camera.orthographicSize = 1;
    }
    #endregion


    #region Set Camera-Modes

    void setCameraModeToFirstPerson()
    {
        camMode = CameraModes.FirstPerson;
        setCameraRotatable(true);
        charCon.enabled = true;
        camera.orthographic = false;

        cameraModeText.text = "Current Mode: First Person\n" +
            "<2> - Free Movement\n" +
            "<3> - Edit the Map and Obstacles";
        uiManger.hideSettingsUi();

        foreach (MeshRenderer renderer in meshRenderers) renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        obstacleGenerator.showObsatcleLines(false);
    }

    void setCameraModeToFlyingPerson()
    {
        camMode = CameraModes.FlyingPerson;
        setCameraRotatable(true);
        charCon.enabled = false;
        camera.orthographic = false;

        cameraModeText.text = "Current Mode: Free Movement\n" +
            "<1> - First Person\n" +
            "<3> - Edit the Map and Obstacles";
        uiManger.hideSettingsUi();

        foreach (MeshRenderer renderer in meshRenderers) renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        obstacleGenerator.showObsatcleLines(false);
    }

    void setCameraModeToTopDownView()
    {
        camMode = CameraModes.TopDownView;
        setCameraRotatable(false);
        charCon.enabled = false;
        camera.orthographic = true;

        Vector2 areaSize = FindObjectOfType<BorderGenerator>().areaSize;
        transform.position = new Vector3(areaSize.x / 2 + (areaSize.x / 9), 20, areaSize.y / 2);
        camera.transform.rotation = Quaternion.Euler(90, 0, 0);
        camera.orthographicSize = areaSize.y / 2 + 2;

        cameraModeText.text = "Current Mode: Edit Map\n" +
            "<1> - First Person\n" +
            "<2> - Free Movement";
        uiManger.showSettingsUi();

        foreach (MeshRenderer renderer in meshRenderers) renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        obstacleGenerator.showObsatcleLines(true);
    }

    void switchCameraRotatable()
    {
        setCameraRotatable(Cursor.visible);
    }

    void setCameraRotatable(bool isRotatable)
    {
        if (isRotatable)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            //enableMouseText.text = "<Q> - enable Mouse";
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            //enableMouseText.text = "<Q> - enable Rotation";
        }
    }

    #endregion


    // User Functions
    public void resetPos()
    {
        charCon.enabled = false;
        transform.position = new Vector3(1, 0.25f, 1);
        charCon.enabled = true;
    }

    public void isTypingStuff(bool isTyping)
    {
        typingStuff = isTyping;
    }

    public CameraModes cameraMode
    {
        get { return camMode; }
    }

}
public enum CameraModes
{
    FirstPerson, FlyingPerson, TopDownView
}
