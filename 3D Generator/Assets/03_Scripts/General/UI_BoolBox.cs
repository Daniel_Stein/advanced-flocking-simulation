﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_BoolBox : MonoBehaviour
{
    [Header("Sprites")]
    //[SerializeField] Sprite Background_Sprite;
    [SerializeField] Sprite True_Sprite;
    [SerializeField] Sprite False_Sprite;
    [Header("Image References")]
    [SerializeField] Image Foreground_Image;
    //[Header("Bool Box Value")]
    bool boolbox_value;

    public bool Value
    {
        get { return boolbox_value; }
        set
        {
            boolbox_value = value;

            if (value)
            {
                Foreground_Image.sprite = True_Sprite;
            }
            else
            {
                Foreground_Image.sprite = False_Sprite;
            }
        }
    }

}
