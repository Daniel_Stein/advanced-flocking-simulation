﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public abstract class UiManagerBase : MonoBehaviour
{
    [SerializeField] GameObject settings_panel;

    #region Scene and Game Management

    public void loadScene(int sceneNumber)
    {
        SceneManager.LoadScene(sceneNumber);
    }

    public void leaveGame()
    {
        Application.Quit();
    }

    #endregion


    #region Show and Hide Panels

    public void openPanel(GameObject Panel)
    {
        Panel.SetActive(true);
    }

    public void closePanel(GameObject Panel)
    {
        Panel.SetActive(false);
    }

    protected void moveUI(GameObject panel, Vector2 to)
    {
        LeanTween.move(panel, to, 0.3f);
    }



    #endregion


    #region Settings UI

    public void switchSettingsUiPosition()
    {
        if (settings_panel.transform.position.x >= 1920)
        {
            showSettingsUi();
        }
        else
        {
            hideSettingsUi();
        }
    }

    public void showSettingsUi()
    {
        moveUI(settings_panel, new Vector2(1440, 1080));
    }

    public void hideSettingsUi()
    {
        moveUI(settings_panel, new Vector2(1920, 1080));
    }

    #endregion


    protected PlayMode actPlayMode = PlayMode.Editing;
    public virtual PlayMode ActPlayMode
    {
        get { return actPlayMode; }
        set { actPlayMode = value; }
    }
}

public enum PlayMode
{
    Playing, Editing, Generating, None
}